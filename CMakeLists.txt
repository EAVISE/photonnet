cmake_minimum_required (VERSION 3.8)
project (
  photonnet
  VERSION 1.0.0
  HOMEPAGE_URL "https://eavise.gitlab.io/photonnet"
  LANGUAGES CXX
)

# CXX standard
set (CMAKE_CXX_STANDARD 17)
set (CMAKE_CXX_STANDARD_REQUIRED True)

# Cmake should search ./cmake folder as well
set (CMAKE_MODULE_PATH
  "${PROJECT_SOURCE_DIR}/cmake"
  ${CMAKE_MODULE_PATH}
)

# Options
option (DOCS_ONLY "[DO NOT USE - CI ONLY] Disable some check when only building docs" OFF)

# Packages
if (NOT DOCS_ONLY)
  message (STATUS "DOCS mode only activated!")
  find_package (OpenCV REQUIRED)
  find_package (Torch REQUIRED)
endif ()

# Set a default build type if none was specified
set (default_build_type "Release")
if (NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
    message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
    set(CMAKE_BUILD_TYPE "${default_build_type}" CACHE
            STRING "Choose the type of build." FORCE)

    # Set the possible values of build type for cmake-gui
    set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release")
endif ()
message (STATUS "Build type: ${CMAKE_BUILD_TYPE}")

# Flags
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${TORCH_CXX_FLAGS} -Wall -Wextra")
set (CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0")

# Directories
add_subdirectory (src)
add_subdirectory (examples)
add_subdirectory (docs)
