##########
# Settings

SHELL = /bin/bash
.ONESHELL:
.NOTPARALLEL:
.SILENT:
.PHONY: all clean release debug build docs build build-% guard-%


###########
# Variables

py := python
libtorch := $(shell ${py} -c 'import torch;print(torch.utils.cmake_prefix_path)' 2>/dev/null)
cmake_args :=


#########
# Targets

## Default target
##	This will build everything in release mode
all: release build

## Clean build directory (and create directory if it does not exist)
clean:
	mkdir -p build
	rm -rf build/*

## Configure cmake in release mode
release: clean guard-libtorch
	cd build
	cmake \
		-DCMAKE_PREFIX_PATH=${libtorch} \
		-DCMAKE_BUILD_TYPE=RELEASE ${cmake_args} \
		..

## Configure cmake in debug mode
debug: clean guard-libtorch
	cd build
	cmake \
		-DCMAKE_PREFIX_PATH=${libtorch} \
		-DCMAKE_BUILD_TYPE=DEBUG ${cmake_args} \
		..

## Equivalent to running make in build directory
##	This builds all default targets
build:
	cd build
	$(MAKE) --no-print-directory;

## Shortcut to build documentation
docs: build-sphinx

## Install Photonnet
install:
	cd build
	$(MAKE) instal --no-print-directory;

## Pattern to build code
##	Run build-<target> to build <target>
build-%:
	cd build;
	$(MAKE) $* --no-print-directory;

## Pattern to check whether variable is set.
##	Use guard-<variable> as a prerequisite to enforce it has a value
guard-%:
	if [ "${${*}}" = "" ]; then
		echo "Variable '$*' could not be automatically set."
		echo -e "Run your command as: make <command> $*=<value>\n"
		exit 1
	fi
