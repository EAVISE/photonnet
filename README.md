<img src="https://gitlab.com/EAVISE/photonnet/raw/master/docs/static/photonnet-long.png" alt="Logo" width="100%">  

C++ libtorch frontend for lightnet models  
[![Version][version-badge]][release-url]
[![Documentation][doc-badge]][documentation-url]
[![PyTorch][pytorch-badge]][pytorch-url]
<a href="https://ko-fi.com/D1D31LPHE"><img alt="Ko-Fi" src="https://www.ko-fi.com/img/githubbutton_sm.svg" height="20"></a>  
[![VOC][voc-badge]][voc-url]


## Cite
If you use Photonnet in your research, please cite it.
```
@misc{lightnet18,
  author = {Tanguy Ophoff},
  title = {Lightnet: Building Blocks to Recreate Darknet Networks in Pytorch},
  howpublished = {\url{https://gitlab.com/EAVISE/lightnet}},
  year = {2018}
}
```

## Main Contributors
Here is a list of people that made noteworthy contributions and helped to get this project where it stands today!

- [Tanguy Ophoff](https://gitlab.com/0phoff)


[version-badge]: https://img.shields.io/badge/version-1.0.0-007EC6.svg
[release-url]: https://gitlab.com/EAVISE/photonnet/tags
[doc-badge]: https://img.shields.io/badge/-Documentation-9B59B6.svg
[documentation-url]: https://eavise.gitlab.io/photonnet
[pytorch-badge]: https://img.shields.io/badge/PyTorch-1.7.0-F05732.svg
[pytorch-url]: https://pytorch.org
[voc-badge]: https://img.shields.io/badge/repository-Pascal%20VOC-%2300BFD8
[voc-url]: https://gitlab.com/eavise/top/voc
