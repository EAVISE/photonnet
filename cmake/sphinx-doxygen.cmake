###########################################################
### Build doxygen                                       ###
### Usage (fill in values between <>):                  ###
### TODO : FIX This documentation, it is outdated !!    ###
###                                                     ###
### CmakeLists.txt:                                     ###
###   set (DOXYGEN_INPUT_DIR <Doxyfile INPUT>)          ###
###   set (DOXYFILE_IN <path to Doxyfile.in>)           ###
###   set (SPHINXCONF_IN <path to conf.py.in>)          ###
###   set (SPHINX_FILES <all sphinx rst/md files>)      ###
###   include (sphinx-doxygen)                          ###
###                                                     ###
### Doxyfile.in:                                        ###
###   INPUT             = "@DOXYGEN_INPUT_DIR@"         ###
###   OUTPUT_DIRECTORY  = "@DOXYGEN_OUTPUT_DIR@"        ###
###   GENERATE_XML      = YES                           ###
###                                                     ###
### conf.py.in (breathe):                               ###
###   extensions = [                                    ###
###     ...                                             ###
###     'breathe',                                      ###
###   ]                                                 ###
###     "@PROJECT_NAME@": "@DOXYGEN_OUTPUT_DIR@/xml"    ###
###   }                                                 ###
###   templates_path = ['@SPHINX_TEMPLATES_OUT@']       ###
###   html_static_path = ['@SPHINX_STATIC_OUT@']        ###
###                                                     ###
### conf.py.in (autodoc_doxygen):                       ###
###   extensions = [                                    ###
###     ...                                             ###
###     'sphinxcontrib.autodoc_doxygen',                ###
###   ]                                                 ###
###   doxygen_xml = "@DOXYGEN_OUTPUT_DIR@/xml"          ###
###   templates_path = ['@SPHINX_TEMPLATES_OUT@']       ###
###   html_static_path = ['@SPHINX_STATIC_OUT@']        ###
###                                                     ###
###########################################################


###########
# DOXYGEN #
###########
find_package (Doxygen)

# Find all the public headers
file (GLOB_RECURSE PUBLIC_HEADERS ${DOXYGEN_INPUT_DIR}/*.h)

# Variables
set (DOXYFILE_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)
set (DOXYGEN_OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR}/doxygen)
set (DOXYGEN_INDEX_FILE ${DOXYGEN_OUTPUT_DIR}/xml/index.xml)

# Replace variables inside @@ with the current values
configure_file (${DOXYFILE_IN} ${DOXYFILE_OUT} @ONLY)

# This will be the main output of our command
set (DOXYGEN_INDEX_FILE ${CMAKE_CURRENT_SOURCE_DIR}/doxygen/xml/index.xml)

# Doxygen won't create this for us
file (MAKE_DIRECTORY ${DOXYGEN_OUTPUT_DIR})

# Create doxygen target
add_custom_command (
  OUTPUT ${DOXYGEN_INDEX_FILE}
  COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYFILE_OUT}
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  MAIN_DEPENDENCY ${DOXYFILE_OUT}
  DEPENDS ${PUBLIC_HEADERS}
  COMMENT "Generating Doxygen XML"
)
add_custom_target (doxygen DEPENDS ${DOXYGEN_INDEX_FILE})


###########
# SPHINX  #
###########
find_package (Sphinx)

# Variables
set (SPHINX_CONF_OUT ${CMAKE_CURRENT_BINARY_DIR}/conf.py)
set (SPHINX_SOURCE_OUT ${CMAKE_CURRENT_BINARY_DIR}/source)
set (SPHINX_BUILD ${CMAKE_CURRENT_BINARY_DIR}/sphinx)
set (SPHINX_INDEX_FILE ${SPHINX_BUILD}/index.html)

# Copy necessary dirs
if (SPHINX_STATIC_IN)
  set (SPHINX_STATIC_OUT "static")
  execute_process (
    COMMAND ${CMAKE_COMMAND} -E create_symlink
    ${SPHINX_STATIC_IN}
    ${CMAKE_CURRENT_BINARY_DIR}/${SPHINX_STATIC_OUT}
  )
endif ()

if (SPHINX_TEMPLATES_IN)
  set (SPHINX_TEMPLATES_OUT "templates")
  execute_process (
    COMMAND ${CMAKE_COMMAND} -E create_symlink
    ${SPHINX_TEMPLATES_IN}
    ${CMAKE_CURRENT_BINARY_DIR}/${SPHINX_TEMPLATES_OUT}
  )
endif ()

# Replace variables inside @@ with the current values
configure_file (${SPHINX_CONF_IN} ${SPHINX_CONF_OUT} @ONLY)

# Create sphinx target
add_custom_command (
  OUTPUT ${SPHINX_INDEX_FILE}
  COMMAND
    rm -rf ${SPHINX_BUILD}/* &&
    ${CMAKE_COMMAND} -E copy_directory ${SPHINX_SOURCE_IN} ${SPHINX_SOURCE_OUT} &&
    ${SPHINX_EXECUTABLE} -b html -c ${CMAKE_CURRENT_BINARY_DIR} ${SPHINX_SOURCE_OUT} ${SPHINX_BUILD}
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  MAIN_DEPENDENCY ${SPHINX_CONF_OUT}
  DEPENDS ${DOXYGEN_INDEX_FILE} ${SPHINX_FILES}
  COMMENT "Generating Sphinx HTML"
)
add_custom_target (sphinx DEPENDS ${SPHINX_INDEX_FILE})
