# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import os
import sphinx_rtd_theme


# -- Project information -----------------------------------------------------

project = 'Photonnet'
copyright = '2020, EAVISE'
author = 'EAVISE'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = '@PROJECT_VERSION_MAJOR@.@PROJECT_VERSION_MINOR@.@PROJECT_VERSION_PATCH@'
# The full version, including alpha/beta/rc tags.
release = '@PROJECT_VERSION_MAJOR@.@PROJECT_VERSION_MINOR@.@PROJECT_VERSION_PATCH@'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx.ext.napoleon',
    'sphinx.ext.intersphinx',
    'sphinx.ext.mathjax',
    'sphinxcontrib.autodoc_doxygen',
]

doxygen_xml = "@DOXYGEN_OUTPUT_DIR@/xml"
doxygen_defaults = True
todo_include_todos = True
napoleon_use_ivar = True
autodoc_member_order = 'bysource'
intersphinx_mapping = {
    'pytorch': ('https://pytorch.org/docs/stable', None),
    'python': ('https://docs.python.org/3.6', None),
    'brambox': ('https://eavise.gitlab.io/brambox', None),
    'lightnet': ('https://eavise.gitlab.io/lightnet', None),
}

# Add any paths that contain templates here, relative to this directory.
autosummary_generate = True
templates_path = ['@SPHINX_TEMPLATES_OUT@']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
source_parsers = {
    '.md': 'recommonmark.parser.CommonMarkParser',
}
source_suffix = ['.rst', '.md']

# The master toctree document.
master_doc = 'index'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
html_theme_options = {
    'collapse_navigation': False,
    'display_version': True,
    'logo_only': True,
}

html_logo = '@SPHINX_STATIC_OUT@/photonnet.svg'
html_favicon = '@SPHINX_STATIC_OUT@/photonnet.ico'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['@SPHINX_STATIC_OUT@']

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
html_context = {
    'css_files': [
        'https://fonts.googleapis.com/css?family=Lato',
        '_static/custom_theme.css'
    ],
}
