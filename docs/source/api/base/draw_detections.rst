draw_detections
===============

.. autodoxyfunction:: photonnet::draw_detections
   :args:   cv::Mat &, const torch::Tensor &, std::optional<std::vector<std::string>>

.. autodoxyfunction:: photonnet::draw_detections
   :args:   cv::Mat &, const torch::Tensor &, std::string


.. include:: /links.rst
