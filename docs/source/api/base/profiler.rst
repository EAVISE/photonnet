Profiler
========

.. autodoxyclass:: photonnet::Profiler
   :members:

   .. rubric:: Methods

   .. autodoxysummary::
      
      ~photonnet::Profiler::getTimer
      ~photonnet::Profiler::getAutoTimer
      ~photonnet::Profiler::getTotalTime
      ~photonnet::Profiler::getMeanTime


.. autodoxyclass:: photonnet::Profiler::Timer
   :members:

   .. rubric:: Methods

   .. autodoxysummary::
      
      ~photonnet::Profiler::Timer::Timer
      ~photonnet::Profiler::Timer::~Timer
      ~photonnet::Profiler::Timer::report
      ~photonnet::Profiler::Timer::split


.. include:: /links.rst
