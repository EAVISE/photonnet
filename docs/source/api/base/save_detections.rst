save_detections
===============

.. autodoxyfunction:: photonnet::save_detections
   :args:   std::string, torch::Tensor

.. autodoxyfunction:: photonnet::save_detections
   :args:   std::string, std::vector<torch::Tensor>, unsigned int


.. include:: /links.rst
