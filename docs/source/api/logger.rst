Logger
======

This package comes with a custom, zero-overhead logging system. |br|
It contains a few different macros, which you can use for logging when building the code in debug mode, but the lines get completely stripped in release mode.

To use it, you need to include *photonnet/logger.h*, as the macros are not in the main include header.
Afterwards, you can use the following macros:

- DEBUG  (format_string, ...args)
- INFO   (format_string, ...args)
- ERROR  (format_string, ...args)
- ASSERT (expression)

When building and running your code in debug mode,
the library will look at the ``PN_LOGLVL`` environment variable and filter logging calls depending on the value.
If the environment variable is set to *0*, every log message is shown,
if it is set to *1* (default), only info and error logs are shown, etc.
Setting the variable to a number higher than *3* has no effect,
as ``ASSERT`` macro calls are always evaluated in debug mode. |br|
In order to run a executable with the environment variable, run the following command:

.. code:: bash

   PN_LOGLVL=<value> ./path/to/executable arg1 arg2

Once you are finished developing, you can build your code in release mode, which will strip all macros from your code,
including your assert calls.

.. note::
   The photonnet library uses this logging system as well and adds handy messages and asserts in its code.
   However, in order to use these features it is necessary to build the library in debug mode as well!

.. rubric:: Example

.. code:: cpp

   #include <photonnet/logger.h>

   int main(int argc, const char **argv)
   {
      auto a = 3.1415;

      // Note that we do not enter a semicolon after this line, they get added automatically
      DEBUG("Variable A is equal to %f", a)
      INFO("Variable A is equal to %.2f", a)
      ERROR("Variable A is equal to %.1f", a)

      // Assertion
      ASSERT(static_cast<int>(a) == 3)
   }


.. include:: /links.rst
