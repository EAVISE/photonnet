Photonnet
=========

The bulk of the photonnet library lies in its ``photonnet::transform`` namespace,
but there are a few helpful tools under the root ``photonnet`` namespace.


.. toctree::
   :hidden:

   base/draw_detections.rst
   base/read_txt_file.rst
   base/save_detections.rst
   base/profiler.rst


.. autodoxysummary::

   photonnet::draw_detections
   photonnet::read_txt_file
   photonnet::save_detections
   photonnet::Profiler


.. include:: /links.rst
