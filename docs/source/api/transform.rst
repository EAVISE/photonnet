Transform
=========

The ``photonnet::transform`` namespace contains everything related to pre- and post-processing,
which is the main purpose of this library.
It is build up in similar fashion to the :any:`lightnet.data.transform <lightnet.data>` module.

----


Preprocessing
-------------
These transformation modify the image data to fit a certain input dimension.

.. autodoxysummary::
   :toctree: generated
   :template: class.rst

   photonnet::transform::Crop
   photonnet::transform::Letterbox
   photonnet::transform::Pad


----


Postprocessing
--------------

.. _GetBoxes:

GetBoxes
~~~~~~~~
These operators allow you to convert various network output to a common detection box tensor format:

.. math::
   Tensor_{<num\_boxes \, x \, 7>} = \begin{bmatrix}
      batch\_num, x_{tl}, y_{tl}, x_{br}, y_{br}, confidence, class\_id \\
      batch\_num, x_{tl}, y_{tl}, x_{br}, y_{br}, confidence, class\_id \\
      batch\_num, x_{tl}, y_{tl}, x_{br}, y_{br}, confidence, class\_id \\
      ...
   \end{bmatrix}

.. autodoxysummary::
   :toctree: generated
   :template: class.rst

   photonnet::transform::GetDarknetBoxes
   photonnet::transform::GetMultiscaleDarknetBoxes

Filtering
~~~~~~~~~
The following classes allow you to filter output bounding boxes based on some criteria. |br|
They work with the photonnet common detection box `tensor format <#getboxes>`_.

.. autodoxysummary::
   :toctree: generated
   :template: class.rst

   photonnet::transform::NMS
   photonnet::transform::NMSFast

Reverse Fit
~~~~~~~~~~~
These operations cancel the `pre-processing <#preprocessing>`_ operators,
so that the output detections match the coordinates of the original image.

.. autodoxysummary::
   :toctree: generated
   :template: class.rst

   photonnet::transform::ReverseCrop
   photonnet::transform::ReverseLetterbox
   photonnet::transform::ReversePad


----


Others
------
Some random classes and functions that are used in the transform namespace.

.. autodoxysummary::
   :toctree: generated
   :template: class.rst

   photonnet::transform::Compose
   photonnet::transform::IBaseTransform


.. include:: /links.rst
