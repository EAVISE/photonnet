.. Photonnet documentation master file, created by
   sphinx-quickstart on Thu Nov 26 17:07:11 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:gitlab_url: https://gitlab.com/EAVISE/photonnet

Photonnet documentation
=======================
Photonnet is a library for libtorch, the C++ version of the deep learning library PyTorch.
It was created to serve as a C++ frontend for `Lightnet <lightnet_>`_,
and as such contains all the same pre- and post-processing tools to run inference on lightnet networks in C++.


Cite
----
If you use Photonnet in your research, please cite it.

.. code:: bibtex

   @misc{lightnet18,
     author = {Tanguy Ophoff},
     title = {Lightnet: Building Blocks to Recreate Darknet Networks in Pytorch},
     howpublished = {\url{https://gitlab.com/EAVISE/lightnet}},
     year = {2018}
   }


Table of Contents
=================
.. toctree::
   :maxdepth: 1
   :caption: Notes

   notes/01-installation.rst
   notes/02-getting_started.rst
   notes/03-performance.rst

.. toctree::
   :caption: Python

   Lightnet <https://eavise.gitlab.io/lightnet>

.. toctree::
   :maxdepth: 3
   :caption: API
   :includehidden:

   photonnet <api/photonnet>
   photonnet::transform <api/transform>
   logger <api/logger>


Indices and tables
==================

* :ref:`genindex`


.. include:: /links.rst
