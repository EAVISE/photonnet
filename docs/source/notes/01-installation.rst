Installation
============
Before installing Photonnet, make sure you have the following packages installed:

- CMake     3.8+
- OpenCV    (Only tested with 4.4.0)
- LibTorch  1.7+

Photonnet is build with cmake, but we wrote a nice little `Makefile` to abstract everything away from you. |br|
Building photonnet should be as easy as running:

.. code:: bash

   make -j8

However, in order to build photonnet, cmake needs to know the path to the libtorch library.
By default, we find this path by running: ``python -c 'import torch;print(torch.utils.cmake_prefix_path)'``.
This requires pytorch to be installed in your default python path, and might even break then (See FAQ below).
There are thus two options to let our build system know where to find the libtorch library:

.. code:: bash

   # Point to the correct python executable which has pytorch installed
   make py=path/to/your/specific/python-executable

   # Point to the libtorch path yourself
   make libtorch=path/to/libtorch

Once Photonnet is build, you can install it,
so that you can use it in other CMake projects by requiring it with ``find_package(photonnet REQUIRED)``:

.. code:: bash
   
   sudo make install


Detailed Information
--------------------
Our makefile contains various different targets to build parts of the library, when modifying pieces of it.
The default target just runs ``make release build``.

release
   Configure CMake in release mode. Note that this clears your build folder.

debug
   Configure CMake in debug mode. Note that this clears your build folder.

clean
   Clean the build folder. Requires to run ``release`` or ``debug`` again.

build
   Build everything.

build-%
   Build a specific part of the code. Possibilities are: ``build-photonnet``, ``build-examples``, ``build-run_yolo``, etc.

install
   Install the Photonnet library.

docs
   Build the documentation. Actually just a shortcut for ``build-sphinx``. Note that you need both doxygen and sphinx in order to build the documentation.


FAQ
---
Here is a list of frequently asked questions or encountered problems.

- Variable 'libtorch' could not be automatically set.` |br|
  This error means that the script could not find a path to libtorch.
  See the instructions above to point the Makefile to a valid libtorch directory.

- Do I have to build pytorch from source on Jetson devices ? |br|
  No, you can grab prebuild wheels, which also contain the libtorch shared libraries from the `NVidia Forums <https://forums.developer.nvidia.com/t/pytorch-for-jetson-version-1-7-0-now-available/72048>`_

- Linker error: Undefined reference to ``cv::putText`` (or another cv function). |br|
  Your LibTorch is probably using the pre-cxx11 ABI.
  Go to the PyTorch website and download the correct LibTorch version.

- Random output when using :func:`photonnet::save_detections`. |br|
  This could be related to this `github issue <https://github.com/pytorch/pytorch/issues/48451>`_.

.. include:: /links.rst
