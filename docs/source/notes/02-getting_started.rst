Getting Started
===============
Photonnet was created as a C++ runtime alternative to Lightnet.
As such it contains the necessary pre- and post-processing functionality in order to run Lightnet traced models. |br|
In this tutorial, we will quickly go over an example on how to trace a Lightnet model in python and run it with Photonnet in C++.


Tracing Lightnet
----------------
Once you trained a Lightnet model in python, you can easily trace it and save it as a pytorch pickle file for usage in Photonnet.
As an example, we will trace a YoloV2 model.

.. code:: python

   import torch
   import lightnet as ln

   # Load model
   model = ln.models.YoloV2(20)
   model.load('path/to/weights.pt')
   model.eval()

   # Trace model
   input_tensor = torch.rand(1, 3, 416, 416)    # Adapt this to your input dimensions
   traced_model = torch.jit.trace(model, input_tensor)

   # Save traced model
   traced_network.save('path/for/traced_model.pt')


.. warning::
   It is really important to set your model in eval mode before tracing! |br|
   If you do not do this, layers like dropout will still be active and layers like batchnorm will modify their statistics.
   This will result in really hard to track down bugs, where your traced model gives a different result than your original model.


Photonnet Inference
-------------------
Now that we succesfully traced a network, we can use it in photonnet.
Below we will show a quick code example that runs our traced YoloV2 network on an input, but feel free to take a look at the `code examples <https://gitlab.com/EAVISE/photonnet/-/tree/master/examples>`_ of this repository for a more full-fledged script.

.. code:: cpp

   #include <torch/script.h>
   #include <photonnet.h>
   using namespace photonnet;

   int main(int argc, char **argv) {
      // Get model
      torch::jit::script::Module model;
      try {
        model = torch::jit::load("path/to/traced_model.pt");
      }
      catch (const c10::Error& e) {
        std::cerr << "Error loading the model: " << e.msg() << std::endl;
        return -1;
      }
      model.eval();
      model.to(torch::kCUDA);

      // Create pre- and post-processing pipeline.
      tf::Compose pre, post;

      pre
         // Create a Letterbox transform to change input to 416x416
         .add(std::make_unique<transform::Letterbox>(416, 416))
         ;

      post
         // Transform network output to bounding boxes.
         // This special constructor sets up the default lightnet anchor and stride values of YoloV2 automatically
         .add(std::make_unique<transform::GetDarknetBoxes>(transform::GetDarknetBoxes::YoloV2(0.5)))
         // Perform Non-Maximum Suppression
         .add(std::make_unique<transform::NMS>(0.4))
         ;

      // We create a NoGradGuard object, which works like `with torch.no_grad()` in Python
      torch::NoGradGuard no_grad_guard;

      // Create input tensor from an image
      // The preprocessing pipeline can automaticaly return batched tensors and set the right device
      std::vector<torch::jit::IValue> input;
      auto tensor = pre(
         "path/to/image.jpg",
         /*return_batch=*/true,
         /*tensor_options=*/torch::kCUDA,
      );
      input.push_back(tensor);

      // Run the model
      auto model_out = model.forward(input);

      // Run post-processing
      auto output = post(model_out.toTensor());
      output = output.to(torch::kCPU);

      // Show output
      std::cout << output << std::endl;
   }


.. include:: /links.rst
