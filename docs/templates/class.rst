{{ name }}
{{ underline }}

.. autodoxyclass:: {{ fullname }}
   :members:

   {% if methods %}
   .. rubric:: Methods

   .. autodoxysummary::
   {% for item in methods|unique|list %}
      ~{{ fullname }}::{{ item }}
   {%- endfor %}
   {% endif %}

   {% if statics %}
   .. rubric:: Static Methods

   .. autodoxysummary::
   {% for item in statics|unique|list %}
      ~{{ fullname }}::{{ item }}
   {%- endfor %}
   {% endif %}


.. include:: /links.rst
