include_directories (${TARGET_INCLUDE_DIRS})

link_libraries (
  photonnet
  ${TARGET_LIBS}
)

add_executable (run_yolo run_yolo.cpp)
add_executable (validate_yolo validate_yolo.cpp)

set (EXAMPLE_TARGETS
  run_yolo
  validate_yolo
)

# Single target to make it easier to build
if (NOT TARGET examples)
  add_custom_target (examples)
  add_dependencies (examples ${EXAMPLE_TARGETS})
endif()

install (TARGETS
  ${EXAMPLE_TARGETS}
  DESTINATION "bin"
)
