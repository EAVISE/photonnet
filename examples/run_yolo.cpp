#include <opencv2/opencv.hpp>
#include <torch/script.h>
#include <photonnet.h>

using namespace photonnet;


int main(int argc, const char **argv)
{
  // Parse arguments
  std::string keys =
    "{? help            |      | print this message                                           }"
    "{@modelfile        |<none>| libtorch traced model file                                   }"
    "{@image            |<none>| path to the image                                            }"
    "{w width           | 416  | input width for the model                                    }"
    "{h height          | 416  | input height for the model                                   }"
    "{t threshold       | 0.5  | confidence threshold for filtering the output of the model   }"
    "{n nms             | 0.45 | NMS threshold for filtering the output of the model          }"
    "{v version         | 2    | Whether the model is a YoloV2 or YoloV3 derivative           }"
    "{c class_label_map |      | file containing one class_label per row (correct order)      }"
    ;

  cv::CommandLineParser parser(argc, argv, keys);
  parser.about("PhotonNet Yolo Inference Example [v" PN_VERSION "]");
  if (parser.has("help")) {
    parser.printMessage();
    return -1;
  }

  auto model_path = parser.get<std::string>("@modelfile");
  auto img_path = parser.get<std::string>("@image");
  auto width = parser.get<int>("width");
  auto height = parser.get<int>("height");
  auto threshold = parser.get<float>("threshold");
  auto nms = parser.get<float>("nms");
  auto version = parser.get<int>("version");
  auto class_label_map = parser.get<std::string>("class_label_map");

  // Read image
  auto img = cv::imread(img_path);

  // Get model
  torch::jit::script::Module model;
  try {
    model = torch::jit::load(model_path);
  }
  catch (const c10::Error& e) {
    std::cerr << "Error loading the model: " << e.msg() << std::endl;
    return -1;
  }
  model.eval();
  model.to(torch::kCUDA);

  // Create pipelines
  transform::Compose pre, post;
  if (version == 3) {
    pre
      .add(std::make_unique<transform::Letterbox>(width, height))
      ;
    post
      .add(std::make_unique<transform::GetMultiscaleDarknetBoxes>(transform::GetMultiscaleDarknetBoxes::YoloV3(threshold)))
      .add(std::make_unique<transform::NMS>(nms))
      .add(std::make_unique<transform::ReverseLetterbox>(width, height, img.cols, img.rows))
      ;
  } else {
    pre
      .add(std::make_unique<transform::Letterbox>(width, height))
      ;
    post
      .add(std::make_unique<transform::GetDarknetBoxes>(transform::GetDarknetBoxes::YoloV2(threshold)))
      .add(std::make_unique<transform::NMS>(nms))
      .add(std::make_unique<transform::ReverseLetterbox>(width, height, img.cols, img.rows))
      ;
  }

  // Run
  //  Note: We create a NoGradGuard object, which works like `with torch.no_grad()` in Python
  torch::NoGradGuard no_grad_guard;
  Profiler profiler;
  torch::Tensor output;
  {
    auto timer = profiler.getAutoTimer("total");
    
    // Preprocess
    std::vector<torch::jit::IValue> input;
    auto tensor = pre(img, true);
    tensor = tensor.to(torch::kCUDA);
    input.push_back(tensor);
    timer.split("1.pre");
    
    // Run model
    auto model_out = model.forward(input);
    timer.split("2.model");

    // Postprocess
    output = version == 3 ? post(model_out.toTensorVector()) : post(model_out.toTensor());
    output = output.to(torch::kCPU);
    timer.split("3.post");
  }

  // Drawing
  //   Note : The output contains the detection of the entire batch, but since we only run on 1 image,
  //   these are all detections of that image. You might need more filtering if you run on a batch of images.
  if (class_label_map.empty()) {
    draw_detections(img, output);
  } else {
    draw_detections(img, output, class_label_map);
  }

  cv::imshow("result", img);
  cv::waitKey(0);

  // Profile
  std::cout << "\n" << profiler;
}
