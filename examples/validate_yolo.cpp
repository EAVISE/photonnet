#include <opencv2/opencv.hpp>
#include <torch/script.h>
#include <photonnet.h>

using namespace photonnet;
namespace tf = photonnet::transform;


int main(int argc, const char **argv)
{
  // Parse arguments
  std::string keys =
    "{? help            |      | print this message                                           }"
    "{@modelfile        |<none>| libtorch traced model file                                   }"
    "{@data             |<none>| file containing a list of all images (one file per row)      }"
    "{@output           |      | Output file to store pytorch tensor (pytorch pickle format)  }"
    "{w width           | 416  | input width for the model                                    }"
    "{h height          | 416  | input height for the model                                   }"
    "{t threshold       | 0.01 | confidence threshold for filtering the output of the model   }"
    "{n nms             | 0.45 | NMS threshold for filtering the output of the model          }"
    "{v version         | 2    | Whether the model is a YoloV2 or YoloV3 derivative           }"
    ;

  cv::CommandLineParser parser(argc, argv, keys);
  parser.about("PhotonNet Yolo Validation Example [v" PN_VERSION "]");
  if (parser.has("help")) {
    parser.printMessage();
    return -1;
  }

  auto model_path = parser.get<std::string>("@modelfile");
  auto data_path = parser.get<std::string>("@data");
  auto output_path = parser.get<std::string>("@output");
  auto width = parser.get<int>("width");
  auto height = parser.get<int>("height");
  auto threshold = parser.get<float>("threshold");
  auto nms = parser.get<float>("nms");
  auto version = parser.get<int>("version");

  // Get image paths
  std::vector<std::string> data;
  if (!read_txt_file(data_path, data)) {
    std::cerr << "Error loading dataset image paths" << std::endl;
    return -2;
  }

  // Get model
  torch::jit::script::Module model;
  try {
    model = torch::jit::load(model_path);
  }
  catch (const c10::Error& e) {
    std::cerr << "Error loading the model: " << e.msg() << std::endl;
    return -1;
  }
  model.eval();
  model.to(torch::kCUDA);

  // Create pipelines
  //  Note: We assume all images have the same dimensions!
  auto img = cv::imread(data[0]); 
  tf::Compose pre, post;
  if (version == 3) {
    pre
      .add(std::make_unique<tf::Letterbox>(width, height))
      ;
    post
      .add(std::make_unique<tf::GetMultiscaleDarknetBoxes>(tf::GetMultiscaleDarknetBoxes::YoloV3(threshold)))
      .add(std::make_unique<tf::NMS>(nms))
      .add(std::make_unique<tf::ReverseLetterbox>(width, height, img.cols, img.rows))
      ;
  } else {
    pre
      .add(std::make_unique<tf::Letterbox>(width, height))
      ;
    post
      .add(std::make_unique<tf::GetDarknetBoxes>(tf::GetDarknetBoxes::YoloV2(threshold)))
      .add(std::make_unique<tf::NMS>(nms))
      .add(std::make_unique<tf::ReverseLetterbox>(width, height, img.cols, img.rows))
      ;
  }
  // Since all images have same input dim, we can save some time by precomputing the transformation params
  static_cast<tf::Letterbox&>(pre[0]).precompute(img.cols, img.rows);

  // Run
  //  Note: We create a NoGradGuard object, which works like `with torch.no_grad()` in Python
  torch::NoGradGuard no_grad_guard;
  Profiler profiler;
  std::vector<torch::Tensor> output_list;
  output_list.reserve(data.size());

  for (const auto& img_path : data) {
    // Dont include imread in timing results
    auto img = cv::imread(img_path);
    auto timer = profiler.getAutoTimer("total");
    
    // Preprocess
    auto tensor = pre(img, true);
    tensor = tensor.to(torch::kCUDA);
    std::vector<torch::jit::IValue> input;
    input.push_back(tensor);
    timer.split("1.pre");
    
    // Run model
    auto output = model.forward(input);
    timer.split("2.model");

    // Postprocess
    auto post_output = version == 3 ? post(output.toTensorVector()) : post(output.toTensor());
    output_list.push_back(post_output.to(torch::kCPU));
    timer.split("3.post");
  }

  // Save output
  if (!output_path.empty()) {
    save_detections(output_path, output_list, 1);
  }

  // Profile
  std::cout << "\n" << profiler;
}
