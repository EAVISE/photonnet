#pragma once

/* DEBUG MODE */
#ifndef NDEBUG

// Log levels
#define LOG_DEBUG                 0
#define LOG_INFO                  1
#define LOG_ERROR                 2
#define LOG_NONE                  3

// Get log level 
#include <stdlib.h>
#include <stdio.h>
const static char*  PN_LOGLVL_STR = getenv("PN_LOGLVL");
const static int    PN_LOGLVL     = (PN_LOGLVL_STR == nullptr) ? 1 : atoi(PN_LOGLVL_STR);

// Basic printing and formatting
#define PRINT(format, args...)    fprintf(stderr, format, args)
#define NEWLINE                   "\n"
#define __FILE                    strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__
#define TAG_DEBUG                 "DEBUG"
#define TAG_INFO                  "INFO"
#define TAG_ERROR                 "ERROR"
#define TAG_ASSERT                "ASSERT"
#define LOG_FMT                   "%-5s [%-20s %20s:%03d] "
#define LOG_ARGS(TAG)             TAG, __FILE, __FUNCTION__, __LINE__

// Logging macros
#define DEBUG(msg, args...)       { if (PN_LOGLVL <= LOG_DEBUG) PRINT(LOG_FMT " " msg NEWLINE, LOG_ARGS(TAG_DEBUG), ## args); }
#define INFO(msg, args...)        { if (PN_LOGLVL <= LOG_INFO)  PRINT(LOG_FMT msg NEWLINE, LOG_ARGS(TAG_INFO),  ## args); }
#define ERROR(msg, args...)       { if (PN_LOGLVL <= LOG_ERROR) PRINT(LOG_FMT msg NEWLINE, LOG_ARGS(TAG_ERROR), ## args); }
#define ASSERT(expr)              { if (!(expr)) { PRINT(LOG_FMT #expr NEWLINE, LOG_ARGS(TAG_ASSERT)); abort(); }         }


/* RELEASE MODE */
#else

// When building release code, strip all logging calls
#define DEBUG(msg, args...)
#define INFO(msg, args...)
#define ERROR(msg, args...)
#define ASSERT(expr)

#endif
