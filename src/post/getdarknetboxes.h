#pragma once
#include "basetransform.h"

namespace photonnet::transform {

//! Convert output from darknet networks to bounding box tensor.
/*!
 *  This transformation takes the output of YoloV2 and its derivatives
 *  and converts it to the common detection box format.
 */
class GetDarknetBoxes : public IBaseTransform
{
public:
  /*!
   * Constructor
   *
   *  \param  conf_thresh     Confidence threshold to filter detections (number between 0-1)
   *  \param  network_stride  Downsampling factor of the network
   *  \param  anchors         2D tensor representing anchor boxes
   */
  GetDarknetBoxes(float conf_thresh, int network_stride, torch::Tensor anchors);

  /*!
   *  Perform the actual transformation on the network output tensor
   *
   *  \param  input   Tensor of shape <batch, num_anchors*(5+num_classes), height, width>
   *  \return         Transformed output tensor
   */
  torch::Tensor transform(torch::Tensor input) const override;

  /*!
   *  Returns some information about the transformation, similar to repr() in python
   *
   *  \return   String with name and information about this transform
   */
  std::string name() const override
  {
    return "GetDarknetBoxes <" + std::to_string(m_conf_thresh).substr(0,5) + ">";
  }

  //! Return GetDarknetBoxes with default Lightnet values for the model DYolo.
  /*!
   *  <pre>
   *  network_stride = 8
   *  anchors = torch::tensor({
   *    {5.2884, 6.9258},
   *    {12.771, 16.03776},
   *    {20.22348, 32.39568},
   *    {37.88448, 19.36212},
   *    {44.9456, 40.0284}
   *  })
   *  </pre>
   */
  static GetDarknetBoxes DYolo(float conf_thresh = 0.5f)
  {
    return GetDarknetBoxes(
      conf_thresh,
      8,
      torch::tensor({{5.2884, 6.9258}, {12.771, 16.03776}, {20.22348, 32.39568}, {37.88448, 19.36212}, {44.9456, 40.0284}})
    );
  }

  //! Return GetDarknetBoxes with default Lightnet values for the model TinyYoloV2.
  /*!
   *  <pre>
   *  network_stride = 32
   *  anchors = torch::tensor({
   *    {1.08, 1.19},
   *    {3.42, 4.41},
   *    {6.63, 11.38},
   *    {9.42, 5.11},
   *    {16.62, 10.52}
   *  })
   *  </pre>
   */
  static GetDarknetBoxes TinyYoloV2(float conf_thresh = 0.5f)
  {
    return GetDarknetBoxes(
      conf_thresh,
      32,
      torch::tensor({{1.08, 1.19}, {3.42, 4.41}, {6.63, 11.38}, {9.42, 5.11}, {16.62, 10.52}})
    );
  }

  //! Return GetDarknetBoxes with default Lightnet values for the models YoloV2, MobilenetYoloV2, MobileYoloV2 and YoloFusion.
  /*!
   *  <pre>
   *  network_stride = 32
   *  anchors = torch::tensor({
   *    {1.3221, 1.73145},
   *    {3.19275, 4.00944},
   *    {5.05587, 8.09892},
   *    {9.47112, 4.84053},
   *    {11.2364, 10.0071}
   *  })
   *  </pre>
   */
  static GetDarknetBoxes YoloV2(float conf_thresh = 0.5f)
  {
    return GetDarknetBoxes(
      conf_thresh,
      32,
      torch::tensor({{1.3221, 1.73145}, {3.19275, 4.00944}, {5.05587, 8.09892}, {9.47112, 4.84053}, {11.2364, 10.0071}})
    );
  }

  //! Return GetDarknetBoxes with default Lightnet values for the model YoloV2Upsample and MobileYoloV2Upsample.
  /*!
   *  <pre>
   *  network_stride = 16
   *  anchors = torch::tensor({
   *    {2.6442, 3.4629},
   *    {6.3855, 8.01888},
   *    {10.11174, 16.19784},
   *    {18.94224, 9.68106},
   *    {22.4728, 20.0142}
   *  })
   *  </pre>
   */
  static GetDarknetBoxes YoloV2Upsample(float conf_thresh = 0.5f)
  {
    return GetDarknetBoxes(
      conf_thresh,
      16,
      torch::tensor({{2.6442, 3.4629}, {6.3855, 8.01888}, {10.11174, 16.19784}, {18.94224, 9.68106}, {22.4728, 20.0142}})
    );
  }

  //! Return GetDarknetBoxes with default Lightnet values for the model Yolt.
  /*!
   *  <pre>
   *  network_stride = 16
   *  anchors = torch::tensor({
   *    {1.08, 1.19},
   *    {3.42, 4.41},
   *    {6.63, 11.38},
   *    {9.42, 5.11},
   *    {16.62, 10.52}
   *  })
   *  </pre>
   */
  static GetDarknetBoxes Yolt(float conf_thresh = 0.5f)
  {
    return GetDarknetBoxes(
      conf_thresh,
      16,
      torch::tensor({{1.08, 1.19}, {3.42, 4.41}, {6.63, 11.38}, {9.42, 5.11}, {16.62, 10.52}})
    );
  }

  /*!
   *  Return the detection threshold
   *
   *  \return   Detection threshold
   */
  float getThreshold()
  {
    return m_conf_thresh;
  }

  /*!
   *  Set a new detection threshold
   *
   *  \param  conf_thresh   New confidence threshold (number between 0-1)
   */
  void setThreshold(float conf_thresh)
  {
    m_conf_thresh = conf_thresh;
  }

private:
  float m_conf_thresh;
  int m_network_stride;
  torch::Tensor m_anchors;
};

}
