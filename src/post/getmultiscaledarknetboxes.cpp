#include <tuple>
#include "getmultiscaledarknetboxes.h"
#include "logger.h"

using namespace torch::indexing;
namespace photonnet::transform {

GetMultiscaleDarknetBoxes::GetMultiscaleDarknetBoxes(float conf_thresh, torch::Tensor network_strides, torch::Tensor anchors)
  : m_conf_thresh(conf_thresh),
    m_network_strides(network_strides),
    m_anchors(anchors)
{
  ASSERT((m_anchors.ndimension() == 3) && (m_anchors.sizes()[0] == m_network_strides.sizes()[0]) && (m_anchors.sizes()[2] == 2))
  m_anchors /= m_network_strides.index({Ellipsis, None, None});
}


torch::Tensor GetMultiscaleDarknetBoxes::transform(std::vector<torch::Tensor>& input) const
{
  const auto device = input[0].device();
  const auto batch = input[0].sizes()[1];
  const auto channels = input[0].sizes()[2];
  const auto h = input[0].sizes()[3];
  const auto w = input[0].sizes()[4];
  const auto num_anchors = m_anchors.sizes()[1];
  const auto num_classes = (channels / num_anchors) - 5;
  const auto lin_x = torch::linspace(0, w-1, w).repeat({h, 1}).view({h*w}).to(device);
  const auto lin_y = torch::linspace(0, h-1, h).view({h, 1}).repeat({1, w}).view({h*w}).to(device);
  const int  size = input.size();
  ASSERT (size <= m_network_strides.sizes()[0])

  std::vector<torch::Tensor> output_list;
  for (int i=0; i<size; i++) {
    auto anchor_w = m_anchors.index({i, Slice(), 0}).contiguous().view({1, num_anchors, 1}).to(device);
    auto anchor_h = m_anchors.index({i, Slice(), 1}).contiguous().view({1, num_anchors, 1}).to(device);
    torch::Tensor stride_input(input[i]);

    // Comput xc, yc, w, h, box_score
    stride_input = stride_input.view({batch, num_anchors, -1, h*w});
    stride_input.index_put_(
      {Slice(), Slice(), 0, Slice()},
      stride_input.index({Slice(), Slice(), 0, Slice()}).sigmoid_().add_(lin_x).mul_(m_network_strides[i])
    );
    stride_input.index_put_(
      {Slice(), Slice(), 1, Slice()},
      stride_input.index({Slice(), Slice(), 1, Slice()}).sigmoid_().add_(lin_y).mul_(m_network_strides[i])
    );
    stride_input.index_put_(
      {Slice(), Slice(), 2, Slice()},
      stride_input.index({Slice(), Slice(), 2, Slice()}).exp_().mul_(anchor_w).mul_(m_network_strides[i])
    );
    stride_input.index_put_(
      {Slice(), Slice(), 3, Slice()},
      stride_input.index({Slice(), Slice(), 3, Slice()}).exp_().mul_(anchor_h).mul_(m_network_strides[i])
    );
    stride_input.index_put_(
      {Slice(), Slice(), 4, Slice()},
      stride_input.index({Slice(), Slice(), 4, Slice()}).sigmoid_()
    );

    // Compute class_score
    torch::Tensor cls_max, cls_max_idx;
    if (num_classes > 1) {
      DEBUG("Multi-class output")
      auto cls_scores = stride_input.index({Slice(), Slice(), Slice(5, None), Slice()}).softmax(2);
      std::tie(cls_max, cls_max_idx) = cls_scores.max(2);
      cls_max_idx = cls_max_idx.to(torch::kFloat);
      cls_max.mul_(stride_input.index({Slice(), Slice(), 4, Slice()}));
    } else {
      DEBUG("Single-class output")
      cls_max = stride_input.index({Slice(), Slice(), 4, Slice()});
      cls_max_idx = torch::zeros_like(cls_max);
    }

    // Filtered boxes
    auto score_thresh = cls_max.greater(m_conf_thresh);
    if (score_thresh.sum().item<int>() == 0) {
      DEBUG("No boxes after confidence filtering stride %d", i)
      return torch::empty({0, 7}, device);
    }
    DEBUG("%ld/%ld boxes left after confidence filtering stride %d", score_thresh.sum().item<long>(), score_thresh.numel(), i)

    // Mask select boxes > conf_thresh
    auto coords = stride_input.transpose(2, 3).index({Ellipsis, Slice(0, 4)});
    coords = coords.masked_select(score_thresh.unsqueeze(-1)).view({-1, 4});
    coords = torch::cat({
      coords.index({Slice(), Slice(0, 2)}) - (coords.index({Slice(), Slice(2, 4)}) / 2),
      coords.index({Slice(), Slice(0, 2)}) + (coords.index({Slice(), Slice(2, 4)}) / 2)
    }, 1);
    auto scores = cls_max.masked_select(score_thresh);
    auto idx = cls_max_idx.masked_select(score_thresh);

    // Get batch numbers of the detections
    auto batch_num = score_thresh.view({batch, -1});
    auto nums = torch::arange(1, batch+1, torch::dtype(torch::kUInt8).device(device));
    batch_num = (batch_num * nums.index({Slice(), None})).masked_select(batch_num) - 1;

    auto output = torch::cat({
      batch_num.index({Slice(), None}).to(torch::kFloat),
      coords,
      scores.index({Slice(), None}),
      idx.index({Slice(), None})
    }, 1);
    output_list.push_back(output);
  }

  auto output = torch::cat(output_list, 0);
  ASSERT((output.ndimension() == 2) && (output.sizes()[1] == 7))
  return output;
}

};
