#pragma once
#include <array>
#include "basetransform.h"

namespace photonnet::transform {

//! Convert the output from multiple yolo output layers (at different scales) to bounding box tensors.
/*!
 *  This transformation takes the output of YoloV3 and its derivatives
 *  and converts it to the common detection box format.
 */
class GetMultiscaleDarknetBoxes : public IBaseTransform
{
public:
  /*!
   * Constructor
   *
   *  .. note::
   *  Unlike GetDarknetBoxes, the anchors here are defined as multiples of the input dimensions
   *  and not as a multiple of the output dimensions!
   *  This anchor list also has one more dimension, in order to differentiate which anchors belong to which stride.
   *
   *  \param  conf_thresh       Confidence threshold to filter detections (number between 0-1)
   *  \param  network_strides   Downsampling factors of the network
   *  \param  anchors           3D tensor representing anchor boxes at different scaling factors
   */
  GetMultiscaleDarknetBoxes(float conf_thresh, torch::Tensor network_strides, torch::Tensor anchors);

  /*!
   *  Perform the actual transformation on the network output tensor
   *
   *  \param  input   List of tensors of shape <scales, batch, num_anchors*(5+num_classes), height, width>
   *  \return         Transformed output tensor
   */
  torch::Tensor transform(std::vector<torch::Tensor>& input) const override;

  /*!
   *  Returns some information about the transformation, similar to repr() in python
   *
   *  \return   String with name and information about this transform
   */
  std::string name() const override
  {
    return "GetMultiscaleDarknetBoxes <" + std::to_string(m_conf_thresh).substr(0,5) + ">";
  }

  //! Return GetMultiscaleDarknetBoxes with default Lightnet values for the model TinyYoloV3.
  /*!
   *  <pre>
   *  network_strides = torch::tensor({32, 16})
   *  anchors = torch::tensor({
   *    {
   *      {81, 82},
   *      {135, 169},
   *      {344, 319}
   *    },
   *    {
   *      {10, 14},
   *      {23, 27},
   *      {37, 58}
   *    }
   *  })
   *  </pre>
   */
  static GetMultiscaleDarknetBoxes TinyYoloV3(float conf_thresh = 0.5f)
  {
    return GetMultiscaleDarknetBoxes(
      conf_thresh,
      torch::tensor({32, 16}),
      torch::tensor({{{81, 82}, {135, 169}, {344, 319}}, {{10, 14}, {23, 27}, {37, 58}}})
    );
  }

  //! Return GetMultiscaleDarknetBoxes with default Lightnet values for the model YoloV3.
  /*!
   *  <pre>
   *  network_strides = torch::tensor({32, 16, 8})
   *  anchors = torch::tensor({
   *    {
   *      {116, 90},
   *      {156, 198},
   *      {373, 326}
   *    },
   *    {
   *      {30, 61},
   *      {62, 45},
   *      {59, 119}
   *    },
   *    {
   *      {10, 13},
   *      {16, 30},
   *      {33, 23}
   *    }
   *  })
   *  </pre>
   */
  static GetMultiscaleDarknetBoxes YoloV3(float conf_thresh = 0.5f)
  {
    return GetMultiscaleDarknetBoxes(
      conf_thresh,
      torch::tensor({32, 16, 8}),
      torch::tensor({{{116, 90}, {156, 198}, {373, 326}}, {{30, 61}, {62, 45}, {59, 119}}, {{10, 13}, {16, 30}, {33, 23}}})
    );
  }

  /*!
   *  Return the detection threshold
   *
   *  \return   Detection threshold
   */
  float getThreshold()
  {
    return m_conf_thresh;
  }

  /*!
   *  Set a new detection threshold
   *
   *  \param  conf_thresh   New confidence threshold (number between 0-1)
   */
  void setThreshold(float conf_thresh)
  {
    m_conf_thresh = conf_thresh;
  }

private:
  float m_conf_thresh;
  torch::Tensor m_network_strides;
  torch::Tensor m_anchors;
};

}
