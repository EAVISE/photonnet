#pragma once
#include "basetransform.h"

namespace photonnet::transform {

//! Performs non-maximal suppression on the bounding boxes.
/*!
 *  This transformation performs non-maximal suppression on the bounding boxes, filtering boxes with a high overlap.
 *  It works on the photonnet common detection box format, output by the :ref:`GetBoxes` functions.
 */
class NMS : public IBaseTransform
{
public:
  /*!
   * Constructor
   *
   *  .. note::
   * A part of the computation of NMS involve a sequential loop through the boxes.
   * This is quite difficult to implement efficiently on the GPU, using the LibTorch API.
   * We thus concluded empirically that it is usually more efficient to move GPU tensors to the CPU for this part of the computations.
   * By passing false to force_cpu, you can disable this behavior and perform all the computations on the original device of the input tensor.
   *
   *  \param  nms_thresh  IoU threshold to filter detections
   *  \param  class_nms   Whether to perform nms per class
   *  \param  force_cpu   Whether to force a part of the computation on CPU (see Note)
   */
  NMS(float nms_thresh, bool class_nms = true, bool force_cpu = true)
    : m_nms_thresh(nms_thresh),
      m_class_nms(class_nms),
      m_force_cpu(force_cpu)
  {}

  /*!
   *  Perform the actual transformation on the bounding box tensor
   *
   *  \param  input   Tensor of shape <batch, num_anchors*(5+num_classes), height, width>
   *  \return         Transformed output tensor
   */
  torch::Tensor transform(torch::Tensor input) const override;

  /*!
   *  Returns some information about the transformation, similar to repr() in python
   *
   *  \return         String with name and information about this transform
   */
  std::string name() const override
  {
    return "NMS <"
      + std::to_string(m_nms_thresh).substr(0,5)
      + ", class_nms=" + (m_class_nms ? "true" : "false")
      + ">";
  }

  /*!
   *  Return the IoU threshold
   *
   *  \return   IoU threshold for NMS filtering
   */
  float getThreshold()
  {
    return m_nms_thresh;
  }

  /*!
   *  Set a new IoU threshold
   *
   *  \param  nms_thresh  IoU threshold for NMS filtering
   */
  void setThreshold(float nms_thresh)
  {
    m_nms_thresh = nms_thresh;
  }

  /*!
   *  Return whether per-class NMS is enabled.
   *
   *  \return   Whether to perform nms per class
   */
  bool getClassNMS()
  {
    return m_class_nms;
  }

  /*!
   *  Enable or disable per-class NMS.
   *
   *  \param  class_nms   Whether to perform nms per class
   */
  void setClassNMS(bool class_nms)
  {
    m_class_nms = class_nms;
  }

private:
  float m_nms_thresh;
  bool m_class_nms;
  bool m_force_cpu;

  inline torch::Tensor batch_nms(torch::Tensor input, const torch::Device& device) const;
};

}

