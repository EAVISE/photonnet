#include "nmsfast.h"
#include "logger.h"

using namespace torch::indexing;
namespace photonnet::transform {

torch::Tensor NMSFast::transform(torch::Tensor input) const
{
  ASSERT((input.ndimension() == 2) && (input.sizes()[1] == 7))
  if (input.numel() == 0)
    return input;

  const auto device = input.device();
  const auto batches = input.index({Slice(), 0});
  auto keep = torch::empty(batches.sizes(), torch::dtype(torch::kBool).device(device));

  const int max = batches.max().item<int>();
  for (int i=0; i <= max; i++ ) {
    const auto mask = batches.eq(i);
    keep.masked_scatter_(
      mask,
      batch_nms(
        input.masked_select(mask.unsqueeze(1)).view({-1, 7}),
        device
      )
    );
  }

  DEBUG("%ld/%ld boxes left after NMSFast", keep.sum().item<long>(), keep.numel())
  return input.masked_select(keep.unsqueeze(1)).view({-1, 7});
}


inline torch::Tensor NMSFast::batch_nms(torch::Tensor input, const torch::Device& device) const
{
  if (input.numel() == 0) {
    return torch::zeros({input.sizes()[0]}, torch::dtype(torch::kBool).device(device));
  }

  auto coords = input.index({Slice(), Slice(1,5)});
  auto scores = input.index({Slice(), 5});
  
  // Sort coordinates by descending score
  auto order = scores.argsort(0, true);
  auto coord_vec = coords.index({order}).split(1, 1);
  auto x1 = coord_vec[0];
  auto y1 = coord_vec[1];
  auto x2 = coord_vec[2];
  auto y2 = coord_vec[3];

  // Compute dx and dy between each pair of boxes (these mat contain every pair twice...)
  auto dx = (x2.min(x2.t()) - x1.max(x1.t())).clamp_min(0);
  auto dy = (y2.min(y2.t()) - y1.max(y1.t())).clamp_min(0);

  // Compute iou
  auto intersections = dx * dy;
  auto areas = (x2 - x1) * (y2 - y1);
  auto unions = (areas + areas.t()) - intersections;
  auto ious = intersections / unions;

  // Filter based on iou (and class)
  auto conflicting = (ious > m_nms_thresh).triu(1);
  if (m_class_nms) {
    auto classes = input.index({Slice(), 6}).index({order});
    auto same_class = (classes.unsqueeze(0) == classes.unsqueeze(1));
    conflicting &= same_class;
  }

  auto keep = conflicting.sum(0).eq(0);
  return keep.scatter(0, order, keep);
}

};
