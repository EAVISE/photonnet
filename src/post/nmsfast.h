#pragma once
#include "basetransform.h"

namespace photonnet::transform {

//! Faster non-maximal suppression implementation.
/*!
 *  This transformation performs non-maximal suppression on the bounding boxes, filtering boxes with a high overlap.
 *  It works on the photonnet common detection box format, output by the :ref:`GetBoxes` functions.
 *
 *  This faster alternative makes a small “mistake” during NMS computation,
 *  in order to remove a necessary loop in the code, allowing it to run faster.
 *  The speed increase should be mostly notable when performing NMS with PyTorch tensors on the GPU.
 *  For more information about the difference between NMS and NMSFast, check out :class:`lightnet.data.transform.NMSFast`.
 */
class NMSFast : public IBaseTransform
{
public:
  /*!
   * Constructor
   *
   *  \param  nms_thresh  IoU threshold to filter detections
   *  \param  class_nms   Whether to perform nms per class
   */
  NMSFast(float nms_thresh, bool class_nms = true)
    : m_nms_thresh(nms_thresh),
      m_class_nms(class_nms)
  {}

  /*!
   *  Perform the actual transformation on the bounding box tensor
   *
   *  \param  input   Tensor of shape <batch, num_anchors*(5+num_classes), height, width>
   *  \return         Transformed output tensor
   */
  torch::Tensor transform(torch::Tensor input) const override;

  /*!
   *  Returns some information about the transformation, similar to repr() in python
   *
   *  \return         String with name and information about this transform
   */
  std::string name() const override
  {
    return "NMSFast <"
      + std::to_string(m_nms_thresh).substr(0,5)
      + ", class_nms=" + (m_class_nms ? "true" : "false")
      + ">";
  }

  /*!
   *  Return the IoU threshold
   *
   *  \return   IoU threshold for NMS filtering
   */
  float getThreshold()
  {
    return m_nms_thresh;
  }

  /*!
   *  Set a new IoU threshold
   *
   *  \param  nms_thresh  IoU threshold for NMS filtering
   */
  void setThreshold(float nms_thresh)
  {
    m_nms_thresh = nms_thresh;
  }

  /*!
   *  Return whether per-class NMS is enabled.
   *
   *  \return   Whether to perform nms per class
   */
  bool getClassNMS()
  {
    return m_class_nms;
  }

  /*!
   *  Enable or disable per-class NMS.
   *
   *  \param  class_nms   Whether to perform nms per class
   */
  void setClassNMS(bool class_nms)
  {
    m_class_nms = class_nms;
  }

private:
  float m_nms_thresh;
  bool m_class_nms;

  inline torch::Tensor batch_nms(torch::Tensor input, const torch::Device& device) const;
};

}

