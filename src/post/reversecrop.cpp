#include "reversecrop.h"
#include "logger.h"

using namespace torch::indexing;
namespace photonnet::transform {

std::tuple<float, int, int> ReverseCrop::computeParams() const
{
  float scale;
  int crop_w, crop_h;

  if ((static_cast<float>(m_net_width) / m_img_width) >= (static_cast<float>(m_net_height) / m_img_height)) {
    scale = static_cast<float>(m_img_width) / m_net_width;
    crop_w = 0;
    crop_h = static_cast<int>(m_img_height / scale - m_net_height + 0.5) / 2;
  } else {
    scale = static_cast<float>(m_img_height) / m_net_height;
    crop_w = static_cast<int>(m_img_width / scale - m_net_width + 0.5) / 2;
    crop_h = 0;
  }

  return {scale, crop_w, crop_h};
}


torch::Tensor ReverseCrop::transform(torch::Tensor input) const
{
  ASSERT((input.ndimension() == 2) && (input.sizes()[1] == 7))
  if (input.numel() == 0)
    return input;

  // Params
  auto [scale, crop_w, crop_h] = m_params;

  // Crop
  DEBUG("Cropping: (%d, %d)", crop_w, crop_h)
  input.index({Slice(), Slice(1,4,2)}) += crop_w;
  input.index({Slice(), Slice(2,5,2)}) += crop_h;

  // Rescale
  DEBUG("Rescaling: %f", scale)
  input.index({Slice(), Slice(1,5)}) *= scale;

  return input;
}

}

