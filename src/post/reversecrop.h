#pragma once
#include <array>
#include <tuple>
#include "basetransform.h"

namespace photonnet::transform {

//! Performs a reverse Crop operation on the bounding boxes.
/*!
 *  Performs a reverse Crop operation on the bounding boxes,
 *  so that the bounding box coordinates are relative to the original image dimensions.
 */
class ReverseCrop : public IBaseTransform
{
public:
  /*!
   *  Constructor
   *
   *  \param  network_size  The width and height of the images going in the network
   *  \param  image_size    The width and height of the original images
   */
  ReverseCrop(std::array<int, 2> network_size, std::array<int, 2> image_size)
    : ReverseCrop(network_size[0], network_size[1], image_size[0], image_size[1])
  {}

  /*!
   *  Constructor
   *
   *  \param  network_size  The size of the images going in the network (square)
   *  \param  image_size    The size of the original images (square)
   */
  ReverseCrop(int network_size, int image_size)
    : ReverseCrop(network_size, network_size, image_size, image_size)
  {}

  /*!
   *  Constructor
   *
   *  \param  network_w   The width of the images going in the network
   *  \param  network_h   The height of the images going in the network
   *  \param  image_w     The width of the original images
   *  \param  image_h     The height of the original images
   */
  ReverseCrop(int network_w, int network_h, int image_w, int image_h)
    : m_net_width(network_w),
      m_net_height(network_h),
      m_img_width(image_w),
      m_img_height(image_h)
  {
    m_params = computeParams();
  }

  /*!
   *  Perform the actual transformation on the bounding box tensor
   *
   *  \param  input   Tensor of shape <num_boxes, 7>
   *  \return         Transformed output tensor
   */
  torch::Tensor transform(torch::Tensor input) const override;

  /*!
   *  Returns some information about the transformation, similar to repr() in python
   *
   *  \return   String with name and information about this transform
   */
  std::string name() const override
  {
    return "ReverseCrop <"
      + std::to_string(m_net_width) + "x" + std::to_string(m_net_height)
      + " -> "
      + std::to_string(m_img_width) + "x" + std::to_string(m_img_height)
      + ">";
  }

  /*!
   *  Returns the network_size of this transformation
   *
   *  \return   The width and height of the images going in the network
   */
  std::array<int, 2> getNetworkSize()
  {
    return {m_net_width, m_net_height};
  }

  /*!
   * Set the network_size of this transformation
   *
   *  \param  dimension   The width and height of the images going in the network
   */
  void setNetworkSize(std::array<int, 2> dimension)
  {
    setNetworkSize(dimension[0], dimension[1]);
  }

  /*!
   * Set the network_size of this transformation
   *
   *  \param  dimension   The size of the images going in the network (square)
   */
  void setNetworkSize(int dimension)
  {
    setNetworkSize(dimension, dimension);
  }

  /*!
   * Set the network_size of this transformation
   *
   *  \param  width   The width of the images going in the network
   *  \param  height  The height of the images going in the network
   */
  void setNetworkSize(int width, int height)
  {
    m_net_width = width;
    m_net_height = height;
    m_params = computeParams();
  }

  /*!
   *  Returns the image_size of this transformation
   *
   *  \return   The width and height of the original images
   */
  std::array<int, 2> getImageSize()
  {
    return {m_img_width, m_img_height};
  }

  /*!
   * Set the image_size of this transformation
   *
   *  \param  dimension   The width and height of the original images
   */
  void setImageSize(std::array<int, 2> dimension)
  {
    setImageSize(dimension[0], dimension[1]);
  }

  /*!
   * Set the image_size of this transformation
   *
   *  \param  dimension   The size of the original images (square)
   */
  void setImageSize(int dimension)
  {
    setImageSize(dimension, dimension);
  }

  /*!
   * Set the image_size of this transformation
   *
   *  \param  width   The width of the original images
   *  \param  height  The height of the original images
   */
  void setImageSize(int width, int height)
  {
    m_img_width = width;
    m_img_height = height;
    m_params = computeParams();
  }

private:
  int m_net_width, m_net_height, m_img_width, m_img_height;
  std::tuple<float, int, int> m_params;

  std::tuple<float, int, int> computeParams() const;
};

}
