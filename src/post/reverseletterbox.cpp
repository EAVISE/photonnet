#include "reverseletterbox.h"
#include "logger.h"

using namespace torch::indexing;
namespace photonnet::transform {

std::tuple<float, int, int> ReverseLetterbox::computeParams() const
{
  float scale;
  int pad_w, pad_h;

  if ((static_cast<float>(m_img_width) / m_net_width) >= (static_cast<float>(m_img_height) / m_net_height)) {
    scale = static_cast<float>(m_img_width) / m_net_width;
  } else {
    scale = static_cast<float>(m_img_height) / m_net_height;
  }
  pad_w = static_cast<int>((m_net_width - static_cast<float>(m_img_width) / scale) / 2);
  pad_h = static_cast<int>((m_net_height - static_cast<float>(m_img_height) / scale) / 2);

  return {scale, pad_w, pad_h};
}


torch::Tensor ReverseLetterbox::transform(torch::Tensor input) const
{
  ASSERT((input.ndimension() == 2) && (input.sizes()[1] == 7))
  if (input.numel() == 0)
    return input;

  // Params
  auto [scale, pad_w, pad_h] = m_params;

  // Pad
  DEBUG("Padding: (%d, %d)", pad_w, pad_h)
  input.index({Slice(), Slice(1,4,2)}) -= pad_w;
  input.index({Slice(), Slice(2,5,2)}) -= pad_h;

  // Rescale
  DEBUG("Rescaling: %f", scale)
  input.index({Slice(), Slice(1,5)}) *= scale;

  return input;
}

}
