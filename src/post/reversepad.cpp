#include "reversepad.h"
#include "logger.h"

using namespace torch::indexing;
namespace photonnet::transform {

std::tuple<int, int> ReversePad::computeParams() const
{
  int pad_w, pad_h;

  pad_w = ((m_net_width - (m_img_width % m_net_width)) % m_net_width) / 2;
  pad_h = ((m_net_height - (m_img_height % m_net_height)) % m_net_height) / 2;

  return {pad_w, pad_h};
}


torch::Tensor ReversePad::transform(torch::Tensor input) const
{
  ASSERT((input.ndimension() == 2) && (input.sizes()[1] == 7))
  if (input.numel() == 0)
    return input;

  // Params
  auto [pad_w, pad_h] = m_params;

  // Pad
  DEBUG("Padding: (%d, %d)", pad_w, pad_h)
  input.index({Slice(), Slice(1,4,2)}) -= pad_w;
  input.index({Slice(), Slice(2,5,2)}) -= pad_h;

  return input;
}

}

