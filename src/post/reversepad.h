#pragma once
#include <array>
#include <tuple>
#include "basetransform.h"

namespace photonnet::transform {

//! Performs a reverse Pad operation on the bounding boxes,
/*!
 *  Performs a reverse Pad operation on the bounding boxes,
 *  so that the bounding box coordinates are relative to the original image dimensions.
 *
 *  .. note::
 *  The network_factor dimensions you pass to this transform are factors and not absolute values.
 *  See Pad for more information.
 */
class ReversePad : public IBaseTransform
{
public:
  /*!
   *  Constructor
   *
   *  \param  network_factor  The width and height factor of the images going in the network
   *  \param  image_size      The width and height of the original images
   */
  ReversePad(std::array<int, 2> network_factor, std::array<int, 2> image_size)
    : ReversePad(network_factor[0], network_factor[1], image_size[0], image_size[1])
  {}

  /*!
   *  Constructor
   *
   *  \param  network_factor  The size factor of the images going in the network (square)
   *  \param  image_size      The size of the original images (square)
   */
  ReversePad(int network_factor, int image_size)
    : ReversePad(network_factor, network_factor, image_size, image_size)
  {}

  /*!
   *  Constructor
   *
   *  \param  network_factor_w  The width of the images going in the network
   *  \param  network_factor_h  The height of the images going in the network
   *  \param  image_w           The width of the original images
   *  \param  image_h           The height of the original images
   */
  ReversePad(int network_factor_w, int network_factor_h, int image_w, int image_h)
    : m_net_width(network_factor_w),
      m_net_height(network_factor_h),
      m_img_width(image_w),
      m_img_height(image_h)
  {
    m_params = computeParams();
  }

  /*!
   *  Perform the actual transformation on the bounding box tensor
   *
   *  \param  input   Tensor of shape <num_boxes, 7>
   *  \return         Transformed output tensor
   */
  torch::Tensor transform(torch::Tensor input) const override;

  /*!
   *  Returns some information about the transformation, similar to repr() in python
   *
   *  \return   String with name and information about this transform
   */
  std::string name() const override
  {
    return "ReversePad <" + std::to_string(m_img_width) + "x" + std::to_string(m_img_height) + ">";
  }

  /*!
   *  Returns the network_factor of this transformation
   *
   *  \return   The width and height factor of the images going in the network
   */
  std::array<int, 2> getNetworkSize()
  {
    return {m_net_width, m_net_height};
  }

  /*!
   * Set the network_factor of this transformation
   *
   *  \param  dimension   The width and height factor of the images going in the network
   */
  void setNetworkSize(std::array<int, 2> dimension)
  {
    setNetworkSize(dimension[0], dimension[1]);
  }

  /*!
   * Set the network_factor of this transformation
   *
   *  \param  dimension   The size factor of the images going in the network (square)
   */
  void setNetworkSize(int dimension)
  {
    setNetworkSize(dimension, dimension);
  }

  /*!
   * Set the network_factor of this transformation
   *
   *  \param  width   The width factor of the images going in the network
   *  \param  height  The height factor of the images going in the network
   */
  void setNetworkSize(int width, int height)
  {
    m_net_width = width;
    m_net_height = height;
    m_params = computeParams();
  }

  /*!
   *  Returns the image_size of this transformation
   *
   *  \return   The width and height of the original images
   */
  std::array<int, 2> getImageSize()
  {
    return {m_img_width, m_img_height};
  }

  /*!
   * Set the image_size of this transformation
   *
   *  \param  dimension   The width and height of the original images
   */
  void setImageSize(std::array<int, 2> dimension)
  {
    setImageSize(dimension[0], dimension[1]);
  }

  /*!
   * Set the image_size of this transformation
   *
   *  \param  dimension   The size of the original images (square)
   */
  void setImageSize(int dimension)
  {
    setImageSize(dimension, dimension);
  }

  /*!
   * Set the image_size of this transformation
   *
   *  \param  width   The width of the original images
   *  \param  height  The height of the original images
   */
  void setImageSize(int width, int height)
  {
    m_img_width = width;
    m_img_height = height;
    m_params = computeParams();
  }

private:
  int m_net_width, m_net_height, m_img_width, m_img_height;
  std::tuple<int, int> m_params;
   
  std::tuple<int, int> computeParams() const;
};

}
