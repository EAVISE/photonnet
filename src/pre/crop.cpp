#include "crop.h"
#include "logger.h"

#include <iostream>

using namespace torch::indexing;
namespace F = torch::nn::functional;
namespace photonnet::transform {

std::tuple<float, int, int> Crop::computeParams(int im_w, int im_h) const
{
  float scale;
  int crop_w, crop_h;

  if ((static_cast<double>(m_width) / im_w) >= (static_cast<double>(m_height) / im_h)) {
    scale = static_cast<double>(m_width) / im_w;
    crop_w = 0;
    crop_h = static_cast<int>(im_h * scale - m_height + 0.5) / 2;
  } else {
    scale = static_cast<double>(m_height) / im_h;
    crop_w = static_cast<int>(im_w * scale - m_width + 0.5) / 2;
    crop_h = 0;
  }

  return {scale, crop_w, crop_h};
}

torch::Tensor Crop::transform(torch::Tensor input) const
{
  ASSERT(input.ndimension() == 4)
  DEBUG("Input tensor <%ldx%ld>", input.sizes()[3], input.sizes()[2])

  // Params
  auto shape = input.sizes();
  auto [scale, crop_w, crop_h] = m_precomputed ? m_params : computeParams(shape[3], shape[2]);

  // Rescale
  DEBUG("Rescaling: %f", scale)
  if (scale != 1) {
    input = F::interpolate(
      input,
      F::InterpolateFuncOptions()
	.size(std::vector<int64_t>({static_cast<int64_t>(shape[2]*scale+0.5), static_cast<int64_t>(shape[3]*scale+0.5)}))
        .mode(torch::kBilinear)
        .align_corners(false)
    );
    input = input.clamp(0, 1);
  }

  // Crop
  DEBUG("Cropping: (%d, %d)", crop_w, crop_h)
  if ((crop_w != 0) || (crop_h != 0)) {
    input = input.index({Ellipsis, Slice(crop_h, crop_h+m_height), Slice(crop_w,crop_w+m_width)});
  }

  DEBUG("Output tensor <%ldx%ld>", input.sizes()[3], input.sizes()[2])
  return input;
}

}
