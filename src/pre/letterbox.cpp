#include "letterbox.h"
#include "logger.h"

namespace F = torch::nn::functional;
namespace photonnet::transform {

std::tuple<float, float, float> Letterbox::computeParams(int im_w, int im_h) const
{
  float scale, pad_w, pad_h;

  if ((static_cast<float>(im_w) / m_width) >= (static_cast<float>(im_h) / m_height)) {
    scale = static_cast<float>(m_width) / im_w;
  } else {
    scale = static_cast<float>(m_height) / im_h;
  }
  pad_w = static_cast<float>(m_width - static_cast<int>(im_w * scale + 0.5)) / 2;
  pad_h = static_cast<float>(m_height - static_cast<int>(im_h * scale + 0.5)) / 2;

  return {scale, pad_w, pad_h};
}


torch::Tensor Letterbox::transform(torch::Tensor input) const
{
  ASSERT(input.ndimension() == 4)
  DEBUG("Input tensor <%ldx%ld>", input.sizes()[3], input.sizes()[2])

  // Params
  auto shape = input.sizes();
  auto [scale, pad_w, pad_h] = m_precomputed ? m_params : computeParams(shape[3], shape[2]);

  // Rescale
  DEBUG("Rescaling: %f", scale)
  if (scale != 1) {
    input = F::interpolate(
      input,
      F::InterpolateFuncOptions()
        .size(std::vector<int64_t>({static_cast<int64_t>(shape[2]*scale+0.5), static_cast<int64_t>(shape[3]*scale+0.5)}))
        .mode(torch::kBilinear)
        .align_corners(false)
    );
    input = input.clamp(0, 1);
  }

  // Pad
  DEBUG("Padding: (%d, %d)", static_cast<int>(pad_w), static_cast<int>(pad_h))
  if ((pad_w != 0) || (pad_h != 0)) {
    input = F::pad(
      input,
      F::PadFuncOptions({
        static_cast<int>(pad_w),
        static_cast<int>(pad_w + .5),
        static_cast<int>(pad_h),
        static_cast<int>(pad_h + .5)
      }).value(m_fill_color)
    );
  }

  DEBUG("Output tensor <%ldx%ld>", input.sizes()[3], input.sizes()[2])
  return input;
}

}
