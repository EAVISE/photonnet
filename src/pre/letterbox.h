#pragma once
#include <array>
#include <tuple>
#include "basetransform.h"

namespace photonnet::transform {

//! Rescale and pad an image tensor to a certain dimension.
/*!
 *  Rescale images and add top/bottom or left/right borders to get to the right network dimensions.
 */
class Letterbox : public IBaseTransform
{
public:
  /*!
   *  Constructor
   *
   *  \param  dimension   Target width and height
   *  \param  fill_color  Color to use to for padding
   */
  Letterbox(std::array<int, 2> dimension, double fill_color = 0.5f)
    : Letterbox(dimension[0], dimension[1], fill_color)
  {}

  /*!
   *  Constructor
   *
   *  \param  dimension   Target size for square network inputs
   *  \param  fill_color  Color to use to for padding
   */
  Letterbox(int dimension, double fill_color = 0.5f)
    : Letterbox(dimension, dimension, fill_color)
  {}

  /*!
   *  Constructor
   *
   *  \param  dimension_w   Target width
   *  \param  dimension_h   Target height
   *  \param  fill_color    Color to use to for padding
   */
  Letterbox(int dimension_w, int dimension_h, double fill_color = 0.5f)
    : m_width(dimension_w), m_height(dimension_h), m_fill_color(fill_color)
  {}

  /*!
   *  Perform the actual transformation on an image tensor
   *
   *  \param  input   Image tensor of shape <batch, channel, height, width>
   *  \return         Transformed output tensor
   */
  torch::Tensor transform(torch::Tensor input) const override;

  /*!
   *  Returns some information about the transformation, similar to repr() in python
   *
   *  \return   String with name and information about this transform
   */
  std::string name() const override
  {
    return "Letterbox <" + std::to_string(m_width) + "x" + std::to_string(m_height) + ">";
  }

  /*!
   *  Precompute the transformation parameters for a certain image width and heigth.
   *
   *  .. note::
   *  The precomputed transformation will only work if
   *  you pass images of the same input dimensions as you passed to this method.
   *  
   *  \param  image_size  Width and height of the input to this transformation.
   */
  void precompute(std::array<int, 2> image_size)
  {
    precompute(image_size[0], image_size[1]);
  }

  /*!
   *  Precompute the transformation parameters for a certain image width and heigth.
   *
   *  .. note::
   *  The precomputed transformation will only work if
   *  you pass images of the same input dimensions as you passed to this method.
   *  
   *  \param  image_size  Width and height of the input for square images
   */
  void precompute(int image_size)
  {
    precompute(image_size, image_size);
  }

  /*!
   *  Precompute the transformation parameters for a certain image width and heigth.
   *
   *  .. note::
   *  The precomputed transformation will only work if
   *  you pass images of the same input dimensions as you passed to this method.
   *  
   *  \param  image_width   Width of the input image
   *  \param  image_height  Height of the input image
   */
  void precompute(int image_width, int image_height)
  {
    m_img_width = image_width;
    m_img_height = image_height;
    m_precomputed = true;
    m_params = computeParams(m_img_width, m_img_height);
  }

  /*!
   *  Return the target dimensions of this transformation
   *
   *  \return   Target width and height
   */
  std::array<int, 2> getDimension()
  {
    return {m_width, m_height};
  }

  /*!
   *  Set the target dimensions of this transformation
   *
   *  \param  dimension   Target width and height
   */
  void setDimension(std::array<int, 2> dimension)
  {
    setDimension(dimension[0], dimension[1]);
  }

  /*!
   *  Set the target dimensions of this transformation
   *
   *  \param  dimension   Target size for square targets
   */
  void setDimension(int dimension)
  {
    setDimension(dimension, dimension);
  }

  /*!
   *  Set the target dimensions of this transformation
   *
   *  \param  width   Target width
   *  \param  height  Target height
   */
  void setDimension(int width, int height)
  {
    m_width = width;
    m_height = height;
    if (m_precomputed)
      m_params = computeParams(m_img_width, m_img_height);
  }

private:
  bool m_precomputed{false};
  int m_width, m_height, m_img_width, m_img_height;
  std::tuple<float, float, float> m_params;
  double m_fill_color;

  std::tuple<float, float, float> computeParams(int img_w, int img_h) const;
};

}
