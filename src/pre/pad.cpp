#include "pad.h"
#include "logger.h"

namespace F = torch::nn::functional;
namespace photonnet::transform {


std::tuple<float, float> Pad::computeParams(int im_w, int im_h) const
{
  float pad_w, pad_h;

  if ((im_w % m_width == 0) && (im_h % m_height == 0)) {
    pad_w = 0;
    pad_h = 0;
  } else {
    pad_w = static_cast<float>((m_width - (im_w % m_width)) % m_width) / 2;
    pad_h = static_cast<float>((m_height - (im_h % m_height)) % m_height) / 2;
  }

  return {pad_w, pad_h};
}

torch::Tensor Pad::transform(torch::Tensor input) const
{
  ASSERT(input.ndimension() == 4)
  DEBUG("Input tensor <%ldx%ld>", input.sizes()[3], input.sizes()[2])

  // Params
  auto shape = input.sizes();
  auto [pad_w, pad_h] = m_precomputed ? m_params : computeParams(shape[3], shape[2]);

  // Pad
  DEBUG("Padding: (%d, %d)", static_cast<int>(pad_w), static_cast<int>(pad_h))
  if ((pad_w != 0) || (pad_h != 0)) {
    input = F::pad(
      input,
      F::PadFuncOptions({
        static_cast<int>(pad_w),
        static_cast<int>(pad_w + .5),
        static_cast<int>(pad_h),
        static_cast<int>(pad_h + .5)
      }).value(m_fill_color)
    );
  }

  DEBUG("Output tensor <%ldx%ld>", input.sizes()[3], input.sizes()[2])
  return input;
}

}
