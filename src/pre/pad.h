#pragma once
#include <array>
#include "basetransform.h"

namespace photonnet::transform {

//! Pad an image tensor to a certain dimension.
/*!
 *  This transformation will pad images,
 *  so that their dimensions are a multiple of a certain target size.
 *
 *  .. note::
 *  The target dimensions you pass to this transform are factors and not absolute values. |br|
 *  As an example, if you pass a target factor of 32 and have an input image of 500x400,
 *  The image tensor will be padded to a dimension of 512x416, as those are the next bigger multiples of 32.
 */
class Pad : public IBaseTransform
{
public:
  /*!
   *  Constructor
   *
   *  \param  dimension_factor  Target width and height factor
   *  \param  fill_color        Color to use to for padding
   */
  Pad(std::array<int, 2> dimension_factor, double fill_color = 0.5f)
    : Pad(dimension_factor[0], dimension_factor[1], fill_color)
  {}

  /*!
   *  Constructor
   *
   *  \param  dimension_factor  Target factor for square network inputs
   *  \param  fill_color        Color to use to for padding
   */
  Pad(int dimension_factor, double fill_color = 0.5f)
    : Pad(dimension_factor, dimension_factor, fill_color)
  {}

  /*!
   *  Constructor
   *
   *  \param  dimension_factor_w  Target width factor
   *  \param  dimension_factor_h  Target height factor
   *  \param  fill_color          Color to use to for padding
   */
  Pad(int dimension_factor_w, int dimension_factor_h, double fill_color = 0.5f)
    : m_width(dimension_factor_w), m_height(dimension_factor_h), m_fill_color(fill_color)
  {}

  /*!
   *  Perform the actual transformation on an image tensor
   *
   *  \param  input   Image tensor of shape <batch, channel, height, width>
   *  \return         Transformed output tensor
   */
  torch::Tensor transform(torch::Tensor input) const override;

  /*!
   *  Returns some information about the transformation, similar to repr() in python
   *
   *  \return   String with name and information about this transform
   */
  std::string name() const override
  {
    return "Pad <" + std::to_string(m_width) + "x" + std::to_string(m_height) + ">";
  }

  /*!
   *  Precompute the transformation parameters for a certain image width and heigth.
   *
   *  .. note::
   *  The precomputed transformation will only work if
   *  you pass images of the same input dimensions as you passed to this method.
   *  
   *  \param  image_size  Width and height of the input to this transformation.
   */
  void precompute(std::array<int, 2> image_size)
  {
    precompute(image_size[0], image_size[1]);
  }

  /*!
   *  Precompute the transformation parameters for a certain image width and heigth.
   *
   *  .. note::
   *  The precomputed transformation will only work if
   *  you pass images of the same input dimensions as you passed to this method.
   *  
   *  \param  image_size  Width and height of the input for square images
   */
  void precompute(int image_size)
  {
    precompute(image_size, image_size);
  }

  /*!
   *  Precompute the transformation parameters for a certain image width and heigth.
   *
   *  .. note::
   *  The precomputed transformation will only work if
   *  you pass images of the same input dimensions as you passed to this method.
   *  
   *  \param  image_width   Width of the input image
   *  \param  image_height  Height of the input image
   */
  void precompute(int image_width, int image_height)
  {
    m_img_width = image_width;
    m_img_height = image_height;
    m_precomputed = true;
    m_params = computeParams(m_img_width, m_img_height);
  }

  /*!
   *  Return the target dimensions of this transformation
   *
   *  \return   Target width and height factor
   */
  std::array<int, 2> getDimension()
  {
    return {m_width, m_height};
  }

  /*!
   *  Set the target dimensions of this transformation
   *
   *  \param  dimension_factor  Target width and height factor
   */
  void setDimension(std::array<int, 2> dimension_factor)
  {
    setDimension(dimension_factor[0], dimension_factor[1]);
  }

  /*!
   *  Set the target dimensions of this transformation
   *
   *  \param  dimension_factor  Target factor for square network inputs
   */
  void setDimension(int dimension_factor)
  {
    setDimension(dimension_factor, dimension_factor);
  }

  /*!
   *  Set the target dimensions of this transformation
   *
   *  \param  width_factor  Target width factor
   *  \param  height_factor Target height factor
   */
  void setDimension(int width_factor, int height_factor)
  {
    m_width = width_factor;
    m_height = height_factor;
    if (m_precomputed)
      m_params = computeParams(m_img_width, m_img_height);
  }

private:
  bool m_precomputed{false};
  int m_width, m_height, m_img_width, m_img_height;
  std::tuple<float, float> m_params;
  double m_fill_color;

  std::tuple<float, float> computeParams(int img_w, int img_h) const;
};

}
