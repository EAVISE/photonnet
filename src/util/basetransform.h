#pragma once
#include <cstdlib>
#include <vector>
#include <string>
#include <iostream>
#include <torch/torch.h>

namespace photonnet::transform {

//! Interface for the pre- and post-processing functions. 
/*!
 *  This class allows to create an object with some case specific settings,
 *  and then call transform() with the data to perform the transformation.
 *
 *  Any transformation that you want to use in a Compose pipeline needs to inherit from this class publicly
 *  and implement the name() method, as well as one of the two transform() implementations.
 */
class IBaseTransform
{
public:
  //! Perform the actual transformation on an image tensor
  /*!
   *  This function can be implemented by the inheriting transform classes. |br|
   *  It gets an input tensor and should return a transformed tensor.
   *
   *  \param  input   Input Tensor
   *  \return         Transformed output tensor
   */
  virtual torch::Tensor transform(torch::Tensor) const
  {
    std::cerr << "[" << name() << "] does not implement transform(torch::Tensor input)" << std::endl;
    abort();
  }

  /*!
   *  This function can be implemented by the inheriting transform classes. |br|
   *  It gets a list of tensors as input and should return a single transformed tensor.
   *
   *  \param  input   List of input tensors
   *  \return         Transformed output tensor
   */
  virtual torch::Tensor transform(std::vector<torch::Tensor>&) const
  {
    std::cerr << "[" << name() << "] does not implement transform(std::vector<torch::Tensor>& input)" << std::endl;
    abort();
  }

  //! Returns some information about the transformation, similar to repr() in python
  /*!
   *  This function should be implemented by the inheriting transform classes. |br|
   *  It returns some information about the transformation, similar to repr() in python.
   *  Note that you should try and return only a single line of text.
   *
   *  \return   String with name and information about this transform
   */
  virtual std::string name() const =0;
};

}
