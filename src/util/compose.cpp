#include <opencv2/imgcodecs.hpp>
#include "compose.h"
#include "logger.h"

namespace photonnet::transform {

Compose& Compose::add(std::unique_ptr<IBaseTransform> transform)
{
  m_transforms.push_back(std::move(transform));
  return *this;
}


torch::Tensor Compose::operator()(const torch::Tensor& input) const
{
  torch::Tensor output(input);

  for (auto&& tf : m_transforms) {
    INFO("Running %s", tf->name().c_str())
    output = tf->transform(output);
  }

  return output;
}


torch::Tensor Compose::operator()(std::vector<torch::Tensor> input) const
{
  INFO("Running %s", m_transforms[0]->name().c_str())
  torch::Tensor output(m_transforms[0]->transform(input));

  for (size_t i=1; i<m_transforms.size(); i++) {
    auto&& tf = m_transforms[i];
    INFO("Running %s", tf->name().c_str())
    output = tf->transform(output);
  }

  return output;
}


torch::Tensor Compose::operator()(
  const cv::Mat& input,
  const bool return_batch,
  const torch::TensorOptions& tensor_options
) const
{
  // Create tensor
  auto tensor = torch::from_blob(
    input.data,
    {input.rows, input.cols, input.channels},
    torch::kUInt8
  ).to(tensor_options);

  // HWC to CHW
  tensor = tensor.permute({2, 0, 1});

  // 0-255 to 0-1
  tensor = tensor / 255;

  // Add batch dimension
  tensor.unsqueeze_(0);

  // Run pipeline
  tensor = operator()(tensor);

  // Remove batch dimension
  if (!return_batch) {
    tensor = tensor.squeeze(0);
  }

  return tensor;
}


torch::Tensor Compose::operator()(
  std::string input,
  const bool return_batch,
  const torch::TensorOptions& tensor_options
) const
{
  // Read RGB image
  auto img = cv::imread(input, cv::IMREAD_COLOR);
  cv::cvtColor(img, img, cv::COLOR_BGR2RGB);

  // Return tensor
  return operator()(img, return_batch, tensor_options);
}


torch::Tensor Compose::operator()(
  const std::vector<cv::Mat>& input,
  const torch::TensorOptions& tensor_options
) const
{
  std::vector<torch::Tensor> input_batch;
  input_batch.reserve(input.size());

  for (const auto& img : input) {
    // Create tensor
    auto tensor = torch::from_blob(
      img.data,
      {img.rows, img.cols, img.channels},
      torch::kUInt8
    ).to(tensor_options);

    // HWC to CHW
    tensor = tensor.permute({2, 0, 1});

    // 0-255 to 0-1
    tensor = tensor / 255;

    input_batch.push_back(tensor);
  }

  return operator()(torch::stack(input_batch));
}


torch::Tensor Compose::operator()(
  const std::vector<std::string>& input,
  const torch::TensorOptions& tensor_options
) const
{
  std::vector<torch::Tensor> input_batch;
  input_batch.reserve(input.size());

  for (const auto& img_path : input) {
    // Read RGB image
    auto img = cv::imread(img_path, cv::IMREAD_COLOR);
    cv::cvtColor(img, img, cv::COLOR_BGR2RGB);

    // Create tensor
    auto tensor = torch::from_blob(
      img.data,
      {img.rows, img.cols, 3},
      torch::kUInt8
    ).to(tensor_options);

    // HWC to CHW
    tensor = tensor.permute({2, 0, 1});

    // 0-255 to 0-1
    tensor = tensor / 255;

    input_batch.push_back(tensor);
  }

  return operator()(torch::stack(input_batch));
}

}
