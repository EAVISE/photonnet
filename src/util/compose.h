#pragma once
#include <string>
#include <vector>
#include <torch/torch.h>
#include <opencv2/core.hpp>
#include "basetransform.h"

namespace photonnet::transform {

//! Combine several transforms together into a single pipeline.
/*! 
This class allows you to combine multiple IBaseTransorm into a single pipeline. |br|
It then allows you to run this pipeline on a torch::Tensor or a cv::Mat object.

.. rubric:: Example

Build a post-processing pipeline and use it on the output of your model.
\Code
  using namespace photonnet;

  // Create post-processing pipeline
  transform::Compose post;
  post
    .add(std::make_unique<transform::GetDarknetBoxes>(transform::GetDarknetBoxes::YoloV2(0.5)))
    .add(std::make_unique<transform::NMS>(0.45))
    .add(std::make_unique<transform::ReverseLetterbox>(416, 416, img.cols, img.rows))
    ;

  // Run pipeline on output
  auto model_out = model.forward(input).toTensor();
  auto post_out = post(model_out);
\EndCode

Some models output lists of tensors, which this compose pipeline can handle as well
(as long as your first transform knows how to handle lists).
\Code
  using namespace photonnet;

  // Create post-processing pipeline
  //  Note the GetMultiscaleDarknetBoxes, which runs on lists of tensors
  transform::Compose post;
  post
    .add(std::make_unique<transform::GetMultiscaleDarknetBoxes>(transform::GetMultiscaleDarknetBoxes::YoloV3(0.5)))
    .add(std::make_unique<transform::NMS>(0.45))
    ;

  // Run pipeline on output
  //  Note that we need toTensorVector() here
  auto model_out = model.forward(input).toTensorVector();
  auto post_out = post(model_out);
\EndCode

Compose can also work with OpenCV images or paths to images, which is useful for pre-processing.
\Code
  using namespace photonnet;

  // Create pre-processing pipeline
  transform::Compose pre;
  pre.add(std::make_unique<transform::Letterbox>(416, 416));

  // Run pipeline on an image
  //  Note that the pre-processing returns a batched tensor, which is usually necessary for running a model
  auto img = cv::imread(img_path);
  cv::cvtColor(img, img, cv::COLOR_BGR2RGB);
  auto tensor = pre(img, true);

  // Run pipeline on multiple images at once
  //  Note that we convert the tensor to IValues, which is necessary for a jit traced model
  std::vector<std::string> images = {"path/to/img1.jpg", "path/to/img2.jpg", "path/to/img3.jpg", "path/to/img4.jpg"};
  auto tensor = pre(img);
  std::vector<torch::jit::IValue> input;
  input.push_back(tensor);
\EndCode
 */
class Compose
{
public:
  /*!
   *  Add an IBaseTransorm to the pipeline.
   *
   *  \param  transform   The transformation you want to add
   *  \return             A reference to this Compose object, allowing you to chain Compose::add() calls.  
   */
  Compose& add(std::unique_ptr<IBaseTransform> transform);

  /*!
   *  Access an IBaseTransform from this Compose pipeline.
   *  
   *  .. note::
   *  The transformation object gets returned as a reference,
   *  but keep in mind that the Compose object keeps the ownership of the transformation through a unique pointer.
   *  One should not try to store the reference and use it after destruction of the pipeline!
   *
   *  \param  index   The zero-based index of the transformation
   *  \return         A reference to the matching transformation
   */
  IBaseTransform& operator[](size_t index) { return *m_transforms.at(index); }

  //! Perform all transformations of your pipeline on a given input.
  /*!
   *  Call the pipeline with an input tensor, and it will loop through all your individual transforms.
   *
   *  \param  input   Input tensor that will be transformed by the composed pipeline
   *  \return         Transformed output tensor
   */
  torch::Tensor operator()(const torch::Tensor& input) const;

  /*!
   *  Call the pipeline with a list of tensors, and it will loop through all your individual transforms.
   *
   *  .. note::
   *  We expect that the first transform to accepts a list of tensors as input and returns a single tensor.
   *
   *  \param  input   List of input tensors that will be transformed by the composed pipeline
   *  \return         Transformed output tensor
   */
  torch::Tensor operator()(std::vector<torch::Tensor> input) const;

  /*!
   *  Transform an opencv image to a torch tensor and apply the transformation pipeline.
   *
   *  .. warning::
   *  This method does not do any image conversions and leaves the images as is.
   *  It is up to the user to perform the necessary conversions (eg. BGR2RGB).
   *
   *  \param  input           Input image
   *  \param  return_batch    Whether to return a batched tensor
   *  \param  tensor_options  TensorOptions to transform tensor before running the pipeline
   *  \return                 Transformed output tensor
   */
  torch::Tensor operator()(
    const cv::Mat& input,
    const bool return_batch = false,
    const torch::TensorOptions& tensor_options = torch::dtype(torch::kFloat32).device(torch::kCPU)
  ) const;

  /*!
   *  Transform an opencv image to a torch tensor and apply the transformation pipeline.
   *
   *  .. note::
   *  This method will read the images as color images
   *  and perform the necessary transformation so that the output tensor is
   *  an RGB image tensor in the correct (B)CHW format.
   *
   *  \param  input           Path to the image
   *  \param  return_batch    Whether to return a batched tensor
   *  \param  tensor_options  TensorOptions to transform tensor before running the pipeline
   *  \return                 Transformed output tensor
   */
  torch::Tensor operator()(
    std::string input,
    const bool return_batch = false,
    const torch::TensorOptions& tensor_options = torch::dtype(torch::kFloat32).device(torch::kCPU)
  ) const;

  /*!
   *  Transform a batch of opencv image to a torch tensor and apply the transformation pipeline.
   *
   *  .. warning::
   *  This method does not do any image conversions and leaves the images as is.
   *  It is up to the user to perform the necessary conversions (eg. BGR2RGB).
   *  It is also important that all images have the same number of channels, or else the code will crash.
   *
   *  \param  input           List of input images
   *  \param  tensor_options  TensorOptions to transform tensor before running the pipeline
   *  \return                 Transformed output tensor
   */
  torch::Tensor operator()(
    const std::vector<cv::Mat>& input,
    const torch::TensorOptions& tensor_options = torch::dtype(torch::kFloat32).device(torch::kCPU)
  ) const;

  /*!
   *  Transform a batch of opencv image to a torch tensor and apply the transformation pipeline.
   *
   *  .. note::
   *  This method will read the images as color images
   *  and perform the necessary transformation so that the output tensor is
   *  an RGB image tensor in the correct BCHW format.
   *
   *  \param  input           List of paths to images
   *  \param  tensor_options  TensorOptions to transform tensor before running the pipeline
   *  \return                 Transformed output tensor
   */
  torch::Tensor operator()(
    const std::vector<std::string>& input,
    const torch::TensorOptions& tensor_options = torch::dtype(torch::kFloat32).device(torch::kCPU)
  ) const;

private:
  std::vector<std::unique_ptr<IBaseTransform>> m_transforms{};
};

}
