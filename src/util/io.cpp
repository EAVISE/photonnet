#include <fstream>
#include <torch/script.h>
#include "io.h"
#include "logger.h"

using namespace torch::indexing;
namespace photonnet {

  bool read_txt_file(std::string path, std::vector<std::string>& data)
  {
    std::ifstream file(path.c_str());
    if (!file) return false;

    std::string line;
    while (std::getline(file, line)) {
      data.push_back(line);
    }

    return true;
  }


  bool save_detections(std::string path, torch::Tensor detections)
  {
    std::ofstream file(path.c_str(), std::ios::out | std::ios::binary);
    if (!file) return false;

    auto bytes = torch::jit::pickle_save(detections.to(torch::kCPU));
    file.write(bytes.data(), bytes.size());

    return true;
  }


  bool save_detections(std::string path, std::vector<torch::Tensor> detections, unsigned int batch_size)
  {
    // Set correct batch_number
    if (batch_size) {
      for (unsigned int i=1; i < detections.size(); i++) {
        detections[i].index_put_(
          {Slice(), 0},
          detections[i].index({Slice(), 0}) + static_cast<int>(i * batch_size)
        );
      }
    }

    return save_detections(path, torch::cat(detections, 0));
  }

}
