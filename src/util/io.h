#pragma once
#include <vector>
#include <string>
#include <torch/torch.h>

namespace photonnet {
  
  /*!
   *  Read a the lines of a UTF-8 text file into a vector of strings.
   *
   *  \param  path  Path to the text file
   *  \param  data  Vector to which we append the lines of the file
   *  \return       Boolean indicating success or failure
   */
  bool read_txt_file(std::string path, std::vector<std::string>& data);

  //! Save detections tensor as a pytorch pickle file.
  /*!
   *  This function moves a tensor to CPU and saves it as a pytorch pickle file.
   *
   *  .. note::
   *  This variant of the function can really be used to save any tensor, regardless of its format.
   *
   *  \param  path        Path to pytorch pickle file
   *  \param  detections  Pytorch tensor to save
   *  \return             Boolean indicating success or failure
   */
  bool save_detections(std::string path, torch::Tensor detections);

  /*!
   *  This function concatenates a list of detection tensors, moves it to CPU and saves it as a pytorch pickle file.
   *
   *  .. note::
   *  If you pass a batch_size, the batch_number values of the tensors will be incremented by :math:`i * batch\_size`,
   *  where :math:`i` is the index of the tensor in the list.
   *  This allows to accumulate the output of a model on different images
   *  and save them with a correct accumulated batch_number. |br|
   *  However, this does require the tensors to be in the common detection format! |br| |br|
   *  If batch_size is set to zero (default), it will simply concatenate the vector in a single tensor and save it. |br|
   *  This can then be used to save any tensor, regardless of its format.
   *
   *  \param  path        Path to pytorch pickle file
   *  \param  detections  List of tensors in the common detection box tensor format
   *  \param  batch_size  The batch_size of the output tensors
   *  \return             Boolean indicating success or failure
   */
  bool save_detections(std::string path, std::vector<torch::Tensor> detections, unsigned int batch_size = 0);
}

