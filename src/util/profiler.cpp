#include "profiler.h"
#include <iomanip>

namespace photonnet {

std::map<std::string, float> Profiler::getTotalTime()
{
  std::map<std::string, float> result;
  for (const auto& [name, value] : m_records) {
    result[name] = value.time.count();
  }

  return result;
}


std::map<std::string, float> Profiler::getMeanTime()
{
  std::map<std::string, float> result;
  for (const auto& [name, value] : m_records) {
    result[name] = value.time.count() / value.count;
  }

  return result;
}


std::ostream& operator<<(std::ostream& out, const Profiler& profiler)
{
  std::ios state(nullptr);
  state.copyfmt(out);
  out << std::fixed << std::setprecision(3);

  out << "========= PROFILER (ms) =========\n";
  out << "name      : total (mean)\n\n";

  for (const auto& [name, value] : profiler.m_records) {
    float time = value.time.count();
    out << std::left << std::setw(10) << name << ": " << time << " (" << time / value.count << ")\n";
  }

  out << "=================================" << std::endl;

  out.copyfmt(state);
  return out;
}

}
