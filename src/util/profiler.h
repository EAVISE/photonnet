#pragma once
#include <string>
#include <map>
#include <chrono>

namespace photonnet {

//! Small profiler capable of measuring code execution times.
/*!
 *  This class is an easy to use profiler to measure code execution times of repeated actions
 *  and compute total and average runtimes.
 *
 *  .. note::
 *  All returned times are floating point numbers, representing milliseconds.
 *
.. rubric:: Example

You can use the profiler to measure certain code lines.
\Code
  using namespace photonnet;

  // Create profiler
  Profiler prof;

  // Measure execution
  auto t1 = prof.getTimer();
  std::cout << "Hello World" << std::endl;
  t1.report("cout");

  auto t2 = prof.getTimer();
  printf("Hello World\n");
  t2.report("printf");

  // Get measured times (ms)
  float cout_time = prof.getTotalTime("cout");
  float printf_time = prog.getTotalTime("printf");

  // Get all times as a map
  std::map<std::string, float> times = prof.getTotalTime();
\EndCode

Where this profiler really shines, is to measure repeated code blocks and report the total and average runtimes.
\Code
  using namespace photonnet;
  Profiler prof;

  // Measure code inside loop (we report a loop entry each time in the loop)
  for (int n : {0, 1, 2, 3, 4, 5}) {
    auto timer = prof.getTimer();
    n = n**2;
    std::cout << n << std::endl;
    timer.report("loop");
  }

  // Get measured time
  float total_time = prof.getTotalTime("loop");
  float mean_time = prof.getMeanTime("loop");

  // Easy reporting through printing
  std::cout << prof;
\EndCode

Finally, this profiler was meant to measure the execution time of running a model,
and thus has a few more bells and whistles:

- An auto timer will automatically report time upon destruction.
- Report intermediate splits inside a code block.
\Code
  using namespace photonnet;
  Profiler prof;
  torch::Tensor output;

  // Create auto timer to measure entire block, and add intermediate splits
  {
    auto timer = profiler.getAutoTimer("total");
    
    // Preprocess
    std::vector<torch::jit::IValue> input;
    auto tensor = pre(img, true);
    tensor = tensor.to(torch::kCUDA);
    input.push_back(tensor);
    timer.split("pre");
    
    // Run model
    auto model_out = model.forward(input);
    // TODO : add CUDA synchronize call
    timer.split("model");

    // Postprocess
    output = post(model_out.toTensor());
    output = output.to(torch::kCPU);
    timer.split("post");

    // Optionally you can call report explicitly, but auto timers do this automatically upon destruction
    //  Note that we call report without arguments!
    // timer.report();
  }

  // Report times
  std::cout << prof;
\EndCode
 */
class Profiler
{
private:
  /*!
   *  Internal profiler class,
   *  that gets returned by the Profiler::getTimer() and Profiler::getAutoTimer() methods.
   */
  class Timer
  {
  public:
    //! Constructor.
    /*!
     *  This constructor creates a non-automatic Timer, that will do nothing by itself.
     *
     *  \param  profiler  Reference to a profiler instance
     */
    Timer(Profiler& profiler)
      : m_profiler(profiler),
        m_reported(true),
        m_start(std::chrono::high_resolution_clock::now()),
        m_split(m_start)
    {}
  
    /*!
     *  Constructor |br|
     *  This constructor creates an automatic Timer,
     *  that will report a timing entry automatically when destructed (if name is not empty).
     *
     *  .. note::
     *  If you call :cpp:func:`report() <void Profiler::Timer::report()>` on an automatic timer, it will not report again when destructed.
     *
     *  \param  profiler  Reference to a profiler instance
     *  \param  name      Name for the automated reporting
     */
    Timer(Profiler& profiler, std::string name)
      : m_profiler(profiler),
        m_name(name),
        m_reported(name.empty()),
        m_start(std::chrono::high_resolution_clock::now()),
        m_split(m_start)
    {}
  
    //! Destructor.
    /*!
     *  When deleting an automatic Timer object, it will report a timing entry,
     *  if :cpp:func:`report() <void Profiler::Timer::report()>` was not called during its lifetime.
     */
    ~Timer()
    {
      if (!m_reported) report();
    }
  
    //! Report a timing entry
    /*!
     *  Report a timing entry with a certain name.
     *
     *  \param  name  Name to classify the timing entry in the Profiler
     */
    inline void report(std::string name)
    {
      m_profiler.m_records[name].count++;
      m_profiler.m_records[name].time += std::chrono::duration<float, std::milli>(
        std::chrono::high_resolution_clock::now() - m_start
      );
    }
  
    /*!
     *  Report a timing entry with the automated name, given in the constructor.
     *
     *  .. note::
     *  If you call this function manually, it will not be called when deleting the automated Timer object.
     */
    inline void report()
    {
      if (!m_name.empty()) {
        m_profiler.m_records[m_name].count++;
        m_profiler.m_records[m_name].time += std::chrono::duration<float, std::milli>(
          std::chrono::high_resolution_clock::now() - m_start
        );
        m_reported = true;
      }
    }

    /*!
     *  Report a timing entry and reset the split timer. |br|
     *  The Timer records 2 timing points, start and split.
     *  Upon creation both are set to the same timepoint, but each time you run this method,
     *  we compute :math:`now() - split`, and reset the split timing point to :math:`now()`.
     *  This allows you to measure a certain code block and also add detailed splits about parts of that code block.
     *
     *  \param  name  Name to classify the timing entry in the Profiler
     */
    inline void split(std::string name)
    {
      auto tmp = std::chrono::high_resolution_clock::now();

      m_profiler.m_records[name].count++;
      m_profiler.m_records[name].time += std::chrono::duration<float, std::milli>(
        tmp - m_split
      );
      m_split = tmp;
    }
  
  private:
    Profiler& m_profiler;
    std::string m_name;
    bool m_reported;
    std::chrono::high_resolution_clock::time_point m_start;
    std::chrono::high_resolution_clock::time_point m_split;
  };

public:
  /*!
   *  Returns a new non-automatic timer instance. |br|
   *  The object you get from this function does not do anything by default.
   *  Use :cpp:func:`report() <void Profiler::Timer::report(std::string)>` or :cpp:func:`split() <Profiler::Timer::split()>` to actually perform timings.
   */
  inline Profiler::Timer getTimer()
  {
    return Timer{*this};
  }

  /*!
   *  Returns a new automatic timer instance. |br|
   *  By default, this timer will measure the length of its lifetime
   *  and report that automatically under the given name.
   *
   *  \param  name  Name for the automated reporting
   */
  inline Profiler::Timer getAutoTimer(std::string name)
  {
    return Timer{*this, name};
  }

  //! Return the accumulated measured time.
  /*!
   *  This method returns a map with different accumulated measured timing intervals.
   *
   *  \return   Map with the total accumulated time per entry.
   */
  std::map<std::string, float> getTotalTime();

  /*!
   *  This method returns the total accumulated time for a given entry.
   *
   *  \param  name  Entry you want the total time for.
   *  \return       Total accumulated time for that entry.
   */
  float getTotalTime(std::string name) { return m_records[name].time.count(); }

  //! Return the mean measured time.
  /*!
   *  This method returns a map with different mean measured timing intervals.
   *
   *  \return   Map with the mean time per entry.
   */
  std::map<std::string, float> getMeanTime();

  /*!
   *  This method returns the mean time for a given entry.
   *
   *  \param  name  Entry you want the mean time for.
   *  \return       Mean time for that entry.
   */
  float getMeanTime(std::string name) { return m_records[name].time.count() / m_records[name].count; }

  /*!
   * TODO
   */
  friend std::ostream& operator<<(std::ostream& out, const Profiler& profiler);
  friend class Timer;

private:
  struct TimingRecord
  {
    std::chrono::duration<float, std::milli> time{0};
    unsigned int count{0};
  };

  std::map<std::string, TimingRecord> m_records;
};

}
