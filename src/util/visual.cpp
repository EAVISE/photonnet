#include <fstream>
#include <opencv2/imgproc.hpp>
#include "io.h"
#include "visual.h"
#include "logger.h"

using namespace torch::indexing;
namespace photonnet {

static const std::vector<cv::Scalar> colors({
  cv::Scalar(255, 255, 102),
  cv::Scalar(102, 255, 224),
  cv::Scalar(239, 102, 255),
  cv::Scalar(102, 239, 255),
  cv::Scalar(255, 102, 178),
  cv::Scalar(193, 102, 255),
  cv::Scalar(255, 102, 224),
  cv::Scalar(102, 193, 255),
  cv::Scalar(255, 102, 132),
  cv::Scalar(117, 255, 102),
  cv::Scalar(255, 163, 102),
  cv::Scalar(102, 255, 178),
  cv::Scalar(209, 255, 102),
  cv::Scalar(163, 255, 102),
  cv::Scalar(255, 209, 102),
  cv::Scalar(102, 147, 255),
  cv::Scalar(147, 102, 255),
  cv::Scalar(102, 255, 132),
  cv::Scalar(255, 117, 102),
  cv::Scalar(102, 102, 255)
});
static const int number_of_colors = colors.size();


bool draw_detections(cv::Mat& image, const torch::Tensor& detections, std::optional<std::vector<std::string>> class_label_map)
{
  INFO("Running Visualisation <%ld detections>", detections.sizes()[0]);
  const int font_face = cv::FONT_HERSHEY_SIMPLEX;
  const double font_scale = 0.5f;
  const int box_thickness = 1;
  const int text_thickness = 1;
  const auto det_num = detections.sizes()[0];

  for (int i=0; i<det_num; i++) {
    const auto det = detections.index({i});
    int idx = det.index({6}).item<int>();
    DEBUG("Drawing detection from batch number %d", det.index({0}).item<int>());

    cv::Point tl(det.index({1}).item<int>(), det.index({2}).item<int>());
    cv::Point br(det.index({3}).item<int>(), det.index({4}).item<int>());
    cv::Scalar color(colors[idx % number_of_colors]);
    std::string text;
    if (class_label_map) {
      text = std::to_string(static_cast<int>(det.index({5}).item<float>() * 100)) + "% " + class_label_map.value()[idx];
    } else {
      text = std::to_string(static_cast<int>(det.index({5}).item<float>() * 100)) + "% " + std::to_string(idx);
    }

    int baseline;
    cv::Size text_size = cv::getTextSize(text, font_face, font_scale, text_thickness, &baseline);
    cv::Point text_orig(
      std::min(image.cols - text_size.width - 1, tl.x),
      std::max(text_size.height, tl.y - baseline)
    );

    cv::rectangle(image, tl, br, color, box_thickness);
    cv::Rect background(text_orig.x, text_orig.y - text_size.height, text_size.width, text_size.height + baseline);
    cv::rectangle(image, background, color, cv::FILLED);
    cv::putText(image, text, text_orig, font_face, font_scale, cv::Scalar(0, 0, 0), text_thickness, cv::LINE_AA);
  }

  return true;
}


bool draw_detections(cv::Mat& image, const torch::Tensor& detections, std::string class_label_map)
{
  std::vector<std::string> class_labels;
  if (!read_txt_file(class_label_map, class_labels)) return false;

  return draw_detections(image, detections, class_labels);
}

}
