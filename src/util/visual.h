#pragma once
#include <vector>
#include <string>
#include <optional>
#include <opencv2/core.hpp>
#include <torch/torch.h>

namespace photonnet {
  
  //! Draw detections on an OpenCV image.
  /*!
   *  This function draws detections on an OpenCV image and adds the class_label and detection score as a text.
   *
   *  .. note::
   *  If you omit the class_label_map, the function will simply print the class IDs
   *
   *  \param  image             OpenCV image that will be drawn on (argument will be modified)
   *  \param  detections        Detections to draw on the image
   *  \param  class_label_map   Map to link integer class IDs to actual names
   *  \return                   Boolean indicating success or failure
   */
  bool draw_detections(
    cv::Mat& image,
    const torch::Tensor& detections,
    std::optional<std::vector<std::string>> class_label_map = std::nullopt
  );

  /*!
   *  This function draws detections on an OpenCV image and adds the class_label and detection score as a text.
   *
   *  \param  image             OpenCV image that will be drawn on (argument will be modified)
   *  \param  detections        Detections to draw on the image
   *  \param  class_label_map   Path to file containing one class_label per line in the correct order
   *  \return                   Boolean indicating success or failure
   */
  bool draw_detections(
    cv::Mat& image,
    const torch::Tensor& detections,
    std::string class_label_map
  );

}
